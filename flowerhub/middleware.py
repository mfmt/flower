"""
The purpose of this middleware is to capture the current
username so we can maintain a modified_by field in all 
runners. Note: The API/restframework does not trigger 
this code, so something equivelant is in views.py
(ApiAuthMixin).
"""

from flowerhub.authorizer import Authz 

class CurrentUserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        Authz.init(getattr(request, 'user', None))
        return self.get_response(request)
