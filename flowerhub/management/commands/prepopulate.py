from django.core.management.base import BaseCommand
import os
from django.contrib.auth import get_user_model
from flowerhub.models import Petal, Partition
from flower.settings import FLOWER_MODE, FLOWER_MODE_DEV, FLOWER_MODE_PRODUCTION, FLOWER_MODE_TEST

class Command(BaseCommand):
    help = "Pre-populate the database with base info for the control panel to work."

    def add_arguments(self, parser):
        parser.add_argument('--tld', help="Top level domain to use for hostnames")

    def handle(self, *args, **options):
        tld = options['tld']
        default_url = None

        if FLOWER_MODE == FLOWER_MODE_DEV:
            if not tld:
                tld = 'dev'       
            # In dev mode, always the same address.
            default_url = 'http://localhost:8888/'

        elif FLOWER_MODE == FLOWER_MODE_PRODUCTION:
            if not tld:
                tld = 'org'

        hostnames = {
            "ldap001.mayfirst." + tld: Petal.LDAP,
            "webstore001.mayfirst." + tld: Petal.WEBSTORE,
            "webproxy001.mayfirst." + tld: Petal.WEBPROXY,
            "mailstore001.mayfirst." + tld: Petal.MAILSTORE,
            "nsauth001.mayfirst." + tld: Petal.NSAUTH,
        }
           
        for hostname in hostnames:
            if default_url:
                url = default_url
            else:
                url = 'https://' + hostname + ':8888/'

            if not Petal.objects.filter(hostname = hostname, category = hostnames[hostname], url = url, is_default=True).exists():
                p = Petal.objects.create(hostname = hostname, category = hostnames[hostname], url = url, is_default=True)
                if hostname == 'webstore001.mayfirst.' + tld or hostname == 'mailstore001.mayfirst.' + tld:
                    # For webstore and mailstore, create a default partition.
                    Partition.objects.create(petal=p, is_default=True)
