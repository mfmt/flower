from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from flower.settings import FLOWER_MODE, FLOWER_MODE_DEV

class Command(BaseCommand):
    help = "Like createsuper user but can be run non-interactively and is idempotent."

    def add_arguments(self, parser):
        username_default = None
        password_default = None
        email_default = None

        if FLOWER_MODE == FLOWER_MODE_DEV:
            # In dev mode, allow simple defaults.
            username_default = 'admin'
            password_default = 'admin'
            email_default = 'admin@example.net'

        parser.add_argument('--username', help="Admin's username", default=username_default)
        parser.add_argument('--email', help="Admin's email", default=email_default)
        parser.add_argument('--password', help="Admin's password", default=password_default)

    def handle(self, *args, **options):
        User = get_user_model()
        if not User.objects.filter(username=options['username']).exists():
            User.objects.create_superuser(username=options['username'],
                                          email=options['email'],
                                          password=options['password'])
