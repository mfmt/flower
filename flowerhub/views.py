from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView, ListView, TemplateView, FormView
from django.views.generic.base import RedirectView
from django.urls import reverse_lazy
from django.utils.translation import gettext as _
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django import forms
from flowerhub.serializers import MemberSerializer, LoginSerializer, EmailMailboxSerializer, DomainNameSerializer, DomainZoneSerializer, MailingListDomainSerializer, WebsiteSerializer, WebsiteDomainNameSerializer, WebsiteLoginSerializer, MysqlDatabaseSerializer, MysqlUserSerializer, AccessSerializer, EmailAliasSerializer
from .forms import DemoForm
from rest_framework import viewsets, mixins
from rest_framework.views import APIView 
from rest_framework.decorators import action
from django.db import connection
from django.urls import reverse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from rest_framework.response import Response
from .menus import FlowerhubMenus
from django.conf import settings
from django.http import JsonResponse
from flowerhub import utils
from flowerhub import shared 
from flowerhub.authorizer import Authz
from django.http import HttpResponseForbidden, HttpResponseRedirect, HttpResponseNotFound, HttpResponse, HttpResponseBadRequest
import re
from django.utils import timezone
import time
import random
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
from flowerhub.models import Member, Login, DomainName, MailingList, Contact, Website, EmailAlias, MysqlDatabase, MysqlUser, Access, EmailMailbox, Petal, Partition, DomainZone, MailingListDomain, MailingListDomainMap, PasswordReset, Feedback, LoginRunner, DomainZoneRunner, EmailMailboxRunner, WebsiteRunner, RunnerCommon, EmailAliasRunner

def get_distinct_webproxy_record_choices(member_id = None, website_id = None ):
    """
    A helper function to populate the domain name select widget used for
    web sites.
    """

    # Somewhat convulated - but django does not have a "not equal to"
    # operator. The goal is: Give me all domain names that are in my
    # membership and of the type WEBPROXY. But... exclude any domain name
    # that is already in use (e.g. a related record exists in the
    # website_domain table) unless it's in use by this web site.
    dn_choices = []

    # All domain names in this membership.
    dns = DomainName.objects.filter(member=member_id)
    # Filter by WEBPROXY
    dns = dns.filter(category=DomainName.WEBPROXY)
    # Exclude any domain names in use except those in use by this site.
    if website_id:
        dns = dns.exclude(website__id__isnull=False, website__id__gt=website_id, website__id__lt=website_id)
    else:
        dns = dns.exclude(website__id__isnull=False)

    for dn in dns:
        dn_choices.append((dn.id, dn))

    return dn_choices

def get_distinct_mx_record_choices(member_id):
    """
    A helper function to populate the domain name select widget used for
    email aliases and email mailboxes.
    """
    domain_name_choices = []
    for domain_name in DomainName.objects.filter(member=member_id, category=DomainName.MX).order_by('domain_zone__domain'):
        fqdn = ""
        if domain_name.label:
            fqdn += domain_name.label + "."
        fqdn += domain_name.domain_zone.domain
        fqdn_tuple = ( fqdn, fqdn )
        if not fqdn_tuple in domain_name_choices:
            domain_name_choices.append( (fqdn, fqdn) )
    return domain_name_choices

# Almost all of our Models are Member Resource models, which means they have
# foreign key relationship to the Member Model. We provide both a
# MemberResourceMixin for all Member Resources as well as a
# MemberResourceFormMixin which is for all Member Resource Forms.
class MemberResourceMixin(SuccessMessageMixin):
    entity_name_display = None
    entity_name = None
    success_message = _("The record was updated successfully.")

    def get_context_data(self, **kwargs):
        member = Member.objects.get(id=self.kwargs['member_pk'])

        # Call the base implementation first to get a context
        c = super().get_context_data(**kwargs)

        pm = FlowerhubMenus()
        # add the request to the context
        c.update({ 
            'member_pk': self.kwargs['member_pk'],
            'member_name': member.name,
            'member_quota': member.get_disk_quota_display(),
            'member_category': member.get_category_display(),
            'menu': pm.get_menu(current_view_name = self.request.get_full_path(), member_pk = self.kwargs['member_pk'])
        })
        return c

class MemberResourceListMixin(UserPassesTestMixin):
    paginate_by = 25 
    order_by = 'id'

    def test_func(self):
        if Authz.has_member_access(self.kwargs['member_pk']):
            return True
                    
        return False 

    # Member resource lists must always restrict records to ones assigned 
    # to the member_pk.
    def get_queryset(self): 
        return self.model.objects.filter(member=self.kwargs['member_pk']).order_by(self.order_by)

class MemberResourceFormMixin(UserPassesTestMixin):
    # The member id field is always populated and hidden on all forms. The code
    # below ensures that the user doesn't muck with it. Be sure to throw a
    # validation error, rather than simply setting it to the correct value.
    # If the value was at any time wrong, we should reject this save because
    # earlier validation methods depend on an accurate member id to determine
    # whether someone has permission to access other values (like domain zone
    # in a domain name record).
    def form_valid(self, form):
        if not form.instance.member.id == self.kwargs['member_pk']:
            form.add_error('member', _("You seem to be mucking with the member id. Don't do that."))
            self.success_message = "" 
            return self.form_invalid(form)

        
        # First, call the parent to save the form.
        response = super().form_valid(form)

        # Now, audit. This code is not really needed now that we have the
        # middleware.CurrentUserMiddleWare but keeping just in case we ever
        # want an extra audit trail.
        model = form.instance.__class__.__name__
        pk = form.instance.id
        user = self.request.user.username
        print("A {0} with id {1} was modified by {2}".format(model, pk, user))
        
        # Now return
        return response

    # Always set the initial value of the member to the value in the URL. This
    # ensures that any validaton on the model level has access to the member id.
    # The form_valid code above happens after all other validation.
    def get_form(self):
        form = super().get_form()
        form.fields['member'].initial = self.kwargs['member_pk']
        return form

    def test_func(self):
        if 'pk' in self.kwargs:
            # We have an object (e.g. update form), make sure they have access
            # to it.
            return Authz.has_member_access(self.get_object().member.id)
        else:
            # The object hasn't been created yet, we are just displaying
            # the form. So, just validate the member_pk in the URL.
            return Authz.has_member_access(self.kwargs['member_pk'])

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        # The purpose here is to display a list of the last 25
        # runners to display in the history tab of the
        # resource form.
        if 'pk' not in self.kwargs:
            # If we are creating a resource, there is no pk and
            # no runners to display.
            return c

        runners = []
        if self.kwargs['pk']:
            entity = self.model.objects.get(pk=self.kwargs['pk'])
            for runner in entity.get_all_runners()[:25]:
                runners.append(runner)

        c.update({ 
            'runners': runners,
            'current_display_status': entity.get_display_status()
        })
        return c

class EnableMixin(MemberResourceMixin, DeleteView):
    """
    This mixin is used for enabling a member resource. It borrows mostly from the
    delete mixin, but overrides the delete function to simply toggle the is_active field.
    """
    model = None 
    context_object_name = 'entity';
    template_name = 'resource_confirm_enable.html'

    # Override delete - don't delete, just enable it.
    def delete(self, request, *args, **kwargs):
        pk = kwargs['pk']
        obj = self.model.objects.get(pk=pk)
        obj.is_active = obj.IS_ACTIVE 
        obj.save()
        success_url = self.get_success_url()
        return HttpResponseRedirect(success_url)

class DisableMixin(MemberResourceMixin, DeleteView):
    """
    This mixin is used for disabling a member resource. It borrows mostly from the
    delete mixin, but overrides the delete function to simply toggle the is_active field.
    """
    model = None 
    context_object_name = 'entity';
    template_name = 'resource_confirm_disable.html'

    # Override delete - don't delete, just disable it.
    def delete(self, request, *args, **kwargs):
        pk = kwargs['pk']
        obj = self.model.objects.get(pk=pk)
        obj.is_active = obj.IS_DISABLED_BY_USER 
        obj.save()
        success_url = self.get_success_url()
        return HttpResponseRedirect(success_url)

class LoginFormMixin():
    """
    This mixin is shared between LoginUpdate and LoginCreate

    """
    def form_valid(self, form):
        # Check to see if we should be sending a password reset link.
        if self.request.POST.get('flower_send_password_reset'):
            email = form.instance.recovery_email
            if not email:
                form.add_error('recovery_email', _("You cannot send a password reset without entering a recovery email."))
                self.success_message = "" 
                return self.form_invalid(form)

            password_reset = PasswordReset(login_id=form.instance.id, notify=True)
            password_reset.save()
            self.success_message = _("The record was updated successfully and email reset notification was not sent to %(email)s because it is not yet implemented." % { 'email': email } )

        return super().form_valid(form)

class WebsiteFormMixin():
    """
    This mixin is shared between WebsiteUpdate and WebsiteCreate. It contains a lot of custom
    code because the Website views are beasts. 
    """

    model = Website
    fields = [ 'member', 'label', 'domain_names', 'logins', 'tls', 'relative_document_root', 'logging', 'php_version', 'settings', 'php_processes', 'max_memory', 'tls_redirect', 'tls_custom', 'cgi', 'proxy_settings', 'website_app', 'website_app_email', 'disk_quota', 'website_store', 'partition' ]

    def form_valid(self, form):
        """
        Get ready, this is complicated. At this point, all validation is complete
        and we have a form.instance.logins containing all the logins that should have
        access to this web site.

        There are two types of logins:
        1. Logins that the current user has control over
        2. Logins that the current user does not have control over (the current user can grant
           access to any login in the system)

        In addition, there are two other types of logins:
        1. Logins that already have shell access
        2. Logins that don't already have shell access.

        Our job is to go over each login. If the login already has shell access, then
        great. We don't have to do anything.

        If the login does not have shell access and this user does not have control
        over the login, then something nefarious is going on and we should return a
        validation error.

        If the login does not have shell access and this user does have control over
        the login, then we silently add shell access to it.
        """ 
        for login in form.cleaned_data.get('logins').all():
            if not login.shell_access:
                if Authz.has_member_access(login.member.id):
                    login.shell_access = True
                    self.success_message += _(" I have granted shell access to {0}.").format(login)
                    login.save()
                else:
                    self.success_message = "" 
                    form.add_error('logins', _("You are trying to add a login that does not have shell access. They won't be able to access your web site with shell."))
                    return self.form_invalid(form)

        # Not done yet! Check if we are supposed to create a database and user

        # If the user signals that we should create a database and user, create them.
        if self.request.POST.get('flower_install_mysql_database'):
            # First, generate the database name and database user name - based
            # on member friendly name and web site label.
            m = Member.objects.get(pk=self.kwargs['member_pk'])

            # Take the first 6 letters from the member friendly name, add an
            # underscore, and then add the first 6 letters from the web site
            # label.
            db_auto_name_base = re.sub('[^a-zA-Z]', '', m.name).lower()[:6] + '_' + re.sub('[^a-zA-Z]', '', form.cleaned_data.get('label')).lower()[:6] 

            # Ensure we don't get a duplicate name.
            attempt = 0
            mysql_database_name = None
            while mysql_database_name == None:
                # Zero pad the end of the name with three digits.
                try_name = db_auto_name_base + '{0:03d}'.format(attempt)
                if not MysqlDatabase.objects.filter(name=try_name).exists():
                    # Great, this name is not in use. 
                    mysql_database_name = try_name
                if attempt > 998:
                    self.success_message = "" 
                    form.add_error(_("I failed to figure out a unique database name. Please create your database and database user manually."))
                    return self.form_invalid(form)
                attempt = attempt + 1

            attempt = 0
            mysql_user_user = None
            while mysql_user_user == None:
                try_name = db_auto_name_base + '{0:03d}'.format(attempt)
                if not MysqlUser.objects.filter(user=try_name).exists():
                    # Great, this will work.
                    mysql_user_user = try_name
                if attempt > 998:
                    self.success_message = "" 
                    form.add_error(_("I failed to figure out a unique database user. Please create your database and database user manually."))
                    return self.form_invalid(form)
                attempt = attempt + 1

            db = MysqlDatabase(name=mysql_database_name, member_id = self.kwargs['member_pk'])
            db.full_clean()
            db.save()

            db_user = MysqlUser(user=mysql_user_user, password=utils.get_random_password(), mysql_database_id=mysql_database_id, member_id = self.kwargs['member_pk'])
            db_user.full_clean()
            db_user.save()

            self.success_message += _(" I have created a database called {0} with a user named {0}.".format(mysql_database_name, mysql_user_user))

        # Now validate fields that depend on whether or not we are super users.
        if not Authz.is_superuser():
            max_memory = form.cleaned_data.get('max_memory')
            if max_memory > Website.USER_LIMIT_MAX_MEMORY:
                self.success_message = "" 
                form.add_error('max_memory', _("Only super users can raise the max memory above {0} MB. Please open a ticket if you need that much memory.".format(Website.USER_LIMIT_MAX_MEMORY)))
                return self.form_invalid(form)

            php_processes = form.cleaned_data.get('php_processes')
            if php_processes > Website.USER_LIMIT_PHP_PROCESSES:
                self.success_message = "" 
                form.add_error('php_processes', _("Only super users can raise the php processes above {0}. Please open a ticket if you need that many processes.".format(Website.USER_LIMIT_PHP_PROCESSES)))
                return self.form_invalid(form)

        
        return super().form_valid(form)

    def get_form(self):
        form = super().get_form()
        
        domain_name_choices = get_distinct_webproxy_record_choices(member_id=self.kwargs['member_pk'], website_id=form.instance.id)
        if not domain_name_choices:
            domain_name_choices = [("", _("No available domain names, please create a new domain using the link above."))]
        form.fields['domain_names'].choices = domain_name_choices 

        # The goal for the logins field is to provide a drop down list of all
        # usernames the current user has access to AND let them type in the
        # complete username of an existing user they don't have access to.

        # Get the currently selected values.
        selected = []
        if form.instance.id:
          for selected_login in form.instance.logins.all():
              selected.append((selected_login.id, selected_login))
        # The queryset argument is used to validate. But choices
        # are limited to the selected items - so we won't give
        # the user a select list that includes all the logins in our
        # system.
        queryset = Login.objects.filter()
        form.fields['logins'] = forms.ModelMultipleChoiceField(queryset, help_text=Website._meta.get_field('logins').help_text)
        # Override selected to just include the ones chosen. The rest are
        # populated by select2.
        form.fields['logins'].choices = selected 

        return form

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        login_view = LoginCreateView()
        login_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        login_view.request = self.request
        login_form = login_view.get_form()

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'login_form': login_form,
            'domain_zone_form': domain_zone_form,
        })
        return c

class SuperUserRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        # Super user has access to all records.
        return Authz.is_superuser()

class HomeView(RedirectView):
    # Redirect based on whether or not we are authenticated.
    def get_redirect_url(self):
        if Authz.is_authenticated():
            return reverse('member-list')
        else:
            if settings.FLOWER_DEMO:
                return reverse('demo')
            else:
                return reverse('welcome')
            
class WelcomeView(TemplateView):
    template_name = 'welcome.html'

    def dispatch(self, *args, **kwargs):
        """
        If the user is logged in, redirect to member list. 
        """
        if Authz.is_authenticated():
            return HttpResponseRedirect(reverse('member-list'))
        else:
            return super().dispatch(*args, **kwargs)

class DemoView(FormView):
    form_class = DemoForm
    template_name = 'demo_form.html'
    
    def build_template_vars(self):
        """
        Call this on both get and post (if post fails validation).
        """
        # Tell the user how long it is before we will reset the data

        # Get the time now. The datetime object will be in UTC, but it will
        # base based on the django set timezone (e.g. in New York with 4 hour
        # offset, it will be a UTC datetime object 4 hours in the future).
        now = timezone.now()

        # Get the offset between UTC and this time zone in hours. Ideally we we
        # get the timezone from Django rather than python, but couldn't figure
        # that out.
        offset = time.localtime().tm_gmtoff/60/60

        # We are aiming for 3:00 am localtime - we have to subtract the offset
        reset_time = now.replace(hour=3 - int(offset), minute=0, second=0)

        if now > reset_time:
            # This means it is after 3:00 am. So, make reset_time tomorrow.
            reset_time = reset_time + timezone.timedelta(days=1)

        time_left = reset_time - now
        time_left_message = "" 
        if time_left.seconds < 900:
            # Less than 15 minutes.
            time_left_message = _("You better hurry!")
        hours, rem = divmod(time_left.seconds, 3600)
        minutes, seconds = divmod(rem, 60)

        return {
            'show_logo': True, 
            'hours_left': hours, 
            'min_left': minutes, 
            'time_left_message': time_left_message 
        }

    def dispatch(self, *args, **kwargs):
        """
        We are not really testing the user but instead the presence of
        a setting.
        """
        if settings.FLOWER_DEMO:
            return super().dispatch(*args, **kwargs)
        else:
          return HttpResponseForbidden()

    def get_random_domain(self):
        verb = [ "fight", "struggle", "build", "construct", "agitate", "revolt", "resist", "demonstrate", "activate", "trundle", "foxtrot", "buck", "clash", "contend", "confront", "engage", "fuss", "makeastand", "riot", "riseup", "strive","vie", "wrangle", "tangle", "zest" ]
        connect = [ "for", "4", "toward", "to" ]
        noun = [ "justice", "peace", "prosperity", "equality", "life", "decency", "fairness", "unity", "harmony", "love", "friendship", "fellowship", "rapport", "serenity", "wisdom", "balance", "wholeness", "health", "wellbeing" ]

        top = [ "net", "org", "coop" ]

        return verb[ random.randint(0, len(verb)-1)] + connect[random.randint(0, len(connect) - 1)] + noun[random.randint(0, len(noun)-1)] + "." + top[random.randint(0, len(top)-1)]

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        form.fields['domain'].initial = self.get_random_domain() 
        template_vars = self.build_template_vars() 
        template_vars['form'] = form
        return render(request, self.template_name, template_vars)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            messages.add_message(request, messages.INFO, _("Your account was created! Please login."))
            member = Member.objects.create(name = form.cleaned_data['membership_name'])
            login = Login.objects.create(first_name = form.cleaned_data['first_name'], username = form.cleaned_data['username'], password = form.cleaned_data['password'], member = member)
            Access.objects.create(member = member, login = login)
            DomainZone.objects.create(domain = form.cleaned_data['domain'], member = member)

            return HttpResponseRedirect(reverse('login'))

        template_vars = self.build_template_vars() 
        template_vars['form'] = form
        return render(request, self.template_name, template_vars)

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        c.update({ 
            'show_logo': True
        })
        return c

# The feedback model/views are for users to provide general feedback about
# the application. Any user can create, only userusers can view, list or edit.
# There is no update view.
class FeedbackListView(SuperUserRequiredMixin, ListView):
    model = Feedback
    context_object_name = 'entity_list'
    template_name = 'feedback_list.html'

class FeedbackCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Feedback 
    template_name = 'feedback_form.html'
    fields = [ 'rank', 'comments', ]
    success_url = reverse_lazy('member-list')
    success_message = _("Thank you for your feedback!")

class FeedbackDeleteView(SuperUserRequiredMixin, DeleteView):
    model = Feedback 
    context_object_name = 'entity'
    template_name = 'feedback_confirm_delete.html'
    success_url = reverse_lazy('feedback-list')

class FeedbackDetailView(SuperUserRequiredMixin, DetailView):
    model = Feedback 
    context_object_name = 'entity'
    template_name = 'feedback_detail.html'

# We start with the Member model, which of course is not a Member Resource. NOTE:
# There is no MemberDetailView - since the member details are shown on all
# Member Resource pages. So, instead of a MemberDetailView we always show the
# default Member Resource list (i.e. Login List).
class MemberListView(LoginRequiredMixin, ListView):
    # Limit list 
    template_name = 'member_list.html'
    context_object_name = 'entity_list'

    def get_queryset(self): 
        return Authz.get_permissioned_queryset(Member)

class MemberUpdateView(SuperUserRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Member
    template_name = 'member_form.html'
    fields = [ 'name', 'disk_quota', 'category' ]
    success_url = reverse_lazy('member-list')
    success_message = _("The membership was updated.")

class MemberCreateView(SuperUserRequiredMixin, SuccessMessageMixin, CreateView):
    model = Member
    template_name = 'member_form.html'
    fields = [ 'name', 'disk_quota', 'category' ]
    success_url = reverse_lazy('member-list')
    success_message = _("The membership was created.")

class MemberDeleteView(SuperUserRequiredMixin, DeleteView):
    model = Member
    template_name = 'member_confirm_delete.html'
    context_object_name = 'entity'
    success_url = reverse_lazy('member-list')

class MemberDetailView(RedirectView):
    pattern_name = 'login-list'
    template_name = 'member_detail.html'
    def get_redirect_url(self, *args, **kwargs):
        # The kwargs will include 'pk' which will be the member_pk.
        # However, to properly dispay login-list, we need that variable
        # to be called member_pk instead, so rebuild the kwargs with the
        # proper name.
        pk = kwargs['pk']
        kwargs = {'member_pk': pk  }
        return super().get_redirect_url(*args, **kwargs)

class MemberDisableView(DeleteView):
    model = Member 
    context_object_name = 'entity';
    template_name = 'member_confirm_disable.html'

    # Override delete - don't delete, just disable it.
    def delete(self, request, *args, **kwargs):
        pk = kwargs['pk']
        obj = self.model.objects.get(pk=pk)
        obj.is_active = False 
        obj.save()
        success_url = self.get_success_url()
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
      return reverse_lazy('member-list' )

class MemberEnableView(DeleteView):
    model = Member 
    context_object_name = 'entity';
    template_name = 'member_confirm_enable.html'

    # Override delete - don't delete, just enable it.
    def delete(self, request, *args, **kwargs):
        pk = kwargs['pk']
        obj = self.model.objects.get(pk=pk)
        obj.is_active = True 
        obj.save()
        success_url = self.get_success_url()
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
      return reverse_lazy('member-list' )

# The Petals and Partition are not member resources and are only accessible by
# super users.

class PetalUpdateView(SuperUserRequiredMixin, UpdateView):
    model = Petal 
    template_name = 'petal_form.html'
    fields = [ 'url', 'hostname', 'category', 'is_default' ]
    success_url = reverse_lazy('petal-list')

class PetalCreateView(SuperUserRequiredMixin, CreateView):
    model = Petal 
    template_name = 'petal_form.html'
    fields = [ 'url', 'hostname', 'category', 'is_default' ]
    success_url = reverse_lazy('petal-list')

class PetalDeleteView(SuperUserRequiredMixin, DeleteView):
    model = Petal 
    template_name = 'confirm_delete.html'
    context_object_name = 'entity'
    success_url = reverse_lazy('petal-list')

class PetalListView(SuperUserRequiredMixin, ListView):
    model = Petal 
    template_name = 'petal_list.html'
    context_object_name = 'entity_list'

class PartitionUpdateView(SuperUserRequiredMixin, UpdateView):
    model = Partition 
    template_name = 'partition_form.html'
    fields = [ 'petal', 'is_default' ]
    success_url = reverse_lazy('partition-list')

class PartitionCreateView(SuperUserRequiredMixin, CreateView):
    model = Partition 
    template_name = 'partition_form.html'
    fields = [ 'petal', 'is_default' ]
    success_url = reverse_lazy('partition-list')

class PartitionDeleteView(SuperUserRequiredMixin, DeleteView):
    model = Partition 
    template_name = 'confirm_delete.html'
    context_object_name = 'entity'
    success_url = reverse_lazy('partition-list')

class PartitionListView(SuperUserRequiredMixin, ListView):
    model = Partition 
    template_name = 'partition_list.html'
    context_object_name = 'entity_list'

# The rest are Member Resources.
class LoginListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = Login 
    template_name = 'login_list.html'
    context_object_name = 'entity_list'
    order_by = 'username'

class LoginUpdateView(MemberResourceMixin, MemberResourceFormMixin, LoginFormMixin, UpdateView):
    model = Login 
    template_name = 'login_form_update.html'
    fields = [ 'member', 'first_name', 'last_name', 'username', 'password', 'recovery_email', 'shell_access', 'ssh_key', 'nextcloud_access', 'nextcloud_quota', 'discourse_access', 'xmpp_access', 'livestreaming_access', 'auto_response_action', 'auto_response_reply_from', 'auto_response', 'mailbox_quota', 'mail_store', 'partition' ]

    def form_valid(self, form):
        if not Authz.is_superuser():
            user_mail_store = form.cleaned_data.get('mail_store')
            if user_mail_store:
                if not self.get_object().mail_store == user_mail_store:
                    self.success_message = "" 
                    form.add_error('mail_store', _("Only super users can change the mail_store."))
                    return self.form_invalid(form)

            user_partition = form.cleaned_data.get('partition')
            if user_partition:
                if not self.get_object().partition == user_partition:
                    self.success_message = "" 
                    form.add_error('partition', _("Only super users can change the partition."))
                    return self.form_invalid(form)

        return super().form_valid(form)

    def get_form(self):
        form = super().get_form()
        # Letting users update their username will cause all kinds of problems.
        form.fields['username'].disabled = True
        return form

    def get_success_url(self):
        return reverse('login-list', args=[self.kwargs['member_pk']])

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'domain_zone_form': domain_zone_form,
            'login_id': self.kwargs['pk'],
            'password': ""
        })
        return c

class LoginCreateView(MemberResourceMixin, MemberResourceFormMixin, LoginFormMixin, CreateView):
    """
    We separate the login create from the login update form because we want the login
    update form to be able to add email addresses. But, you can't add an email address
    until you have a Login id, and you can't have a login id until, well, you have a
    login. So, the create form is a simplified version just asking for the required fields.
    Once you complete it, you are sent to the update form to add more things.
    """
    model = Login 
    template_name = 'login_form_create.html'
    fields = [ 'member', 'first_name', 'last_name', 'username', 'password', 'recovery_email' ]
    success_message = _("Your login was created successful. Please click the Email Settings or Access Settings tab to further configure your login.")

    def get_success_url(self):
        return reverse('login-update', args=[self.kwargs['member_pk'], self.object.id])

class LoginDeleteView(MemberResourceMixin, DeleteView):
    model = Login 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('login-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class LoginDisableView(DisableMixin):
    model = Login 

    def get_success_url(self):
      return reverse_lazy('login-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class LoginEnableView(EnableMixin):
    model = Login 
        
    def get_success_url(self):
      return reverse_lazy('login-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class DomainNameListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = DomainName 
    template_name = 'domain_name_list.html'
    context_object_name = 'entity_list'
    order_by = 'category'

    def get_queryset(self, **kwargs):
        return DomainName.objects.filter(domain_zone = self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        dz = DomainZone.objects.get(id=self.kwargs['pk'])

        runners = []
        for runner in dz.get_all_runners()[:25]:
            runners.append(runner)

        c.update({ 
          'runners': runners,
          'domain_zone_pk': dz.id,
          'domain_zone': dz.domain,
          'current_display_status': dz.get_display_status()
        })
        return c

class DomainNameUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = DomainName 
    template_name = 'domain_name_form.html'
    fields = [ 'member', 'category', 'domain_zone', 'label', 'ip_address', 'ttl', 'server_name', 'txt', 'distance', 'port', 'weight', 'sshfp_algorithm', 'sshfp_type', 'sshfp_data'  ]

    def get_form(self):
        form = super().get_form()
        form.fields['category'].disabled = True
        form.fields['label'].disabled = True
        return form

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        dn = DomainName.objects.get(pk = self.kwargs['pk'])
        dz = DomainZone.objects.get(pk = dn.domain_zone.id)

        domain_name_category_map = DomainName.build_category_map()
        c.update({ 
          'webproxy_default_server_name': settings.FLOWER_WEBPROXY_DEFAULT_SERVER_NAME,
          'domain_name_category_map': domain_name_category_map,
          'domain_zone': dz.domain
        })
        return c

    def get_success_url(self):
        return reverse('domain-name-list', args=[self.kwargs['member_pk'], self.object.domain_zone.id])

class DomainNameCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = DomainName 
    fields = [ 'member', 'category', 'domain_zone', 'label', 'ip_address', 'ttl', 'server_name', 'txt', 'distance', 'port', 'weight', 'member', 'sshfp_algorithm', 'sshfp_type', 'sshfp_data'  ]
    template_name = 'domain_name_form.html'

    def get_form(self):
        form = super().get_form()
        # You must select a category, so remove the empty one.
        form.fields['category'].choices.remove(('', '---------'))
        form.fields['domain_zone'].initial = self.kwargs['domain_zone_pk']
        return form
      
    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        dz = DomainZone.objects.get(pk = self.kwargs['domain_zone_pk'])

        domain_name_category_map = DomainName.build_category_map()
        c.update({ 
          'webproxy_default_server_name': settings.FLOWER_WEBPROXY_DEFAULT_SERVER_NAME,
          'domain_name_category_map': domain_name_category_map,
          'domain_zone': dz.domain
        })
        return c

    def get_success_url(self):
        return reverse('domain-name-list', args=[self.kwargs['member_pk'], self.object.domain_zone.id])

class DomainNameDeleteView(MemberResourceMixin, DeleteView):
    model = DomainName 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('domain-name-list', kwargs = { 'member_pk': self.kwargs['member_pk'], 'pk': self.kwargs['domain_zone_pk'] } )

class DomainNameDisableView(DisableMixin):
    model = DomainName 

    def get_success_url(self):
      return reverse_lazy('domain-name-list', kwargs = { 'member_pk': self.kwargs['member_pk'], 'pk': self.kwargs['domain_zone_pk'] } )

class DomainNameEnableView(EnableMixin):
    model = DomainName 
        
    def get_success_url(self):
        return reverse_lazy('domain-name-list', kwargs = { 'member_pk': self.kwargs['member_pk'], 'pk': self.kwargs['domain_zone_pk'] } )

class DomainZoneListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = DomainZone 
    template_name = 'domain_zone_list.html'
    context_object_name = 'entity_list'
    order_by = 'domain'

class DomainZoneCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = DomainZone 
    fields = [ 'member', 'domain'  ]
    template_name = 'domain_zone_form.html'

class DomainZoneDeleteView(MemberResourceMixin, DeleteView):
    model = DomainZone 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('domain-zone-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class DomainZoneDisableView(DisableMixin):
    model = DomainZone

    def get_success_url(self):
      return reverse_lazy('domain-zone-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class DomainZoneEnableView(EnableMixin):
    model = DomainZone
        
    def get_success_url(self):
        return reverse_lazy('domain-zone-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MailingListDomainListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = MailingListDomain 
    context_object_name = 'entity_list'
    template_name = 'mailing_list_domain_list.html'

class MailingListDomainUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = MailingListDomain 
    template_name = 'mailing_list_domain_form.html'
    fields = [ 'member', 'label', 'domain_zone'  ]

    def get_form(self):
        form = super().get_form()
        form.fields['domain_zone'].queryset = DomainZone.objects.filter(member_id=self.kwargs['member_pk']).order_by('domain')
        return form

    def get_success_url(self):
        return reverse_lazy('mailing-list-domain-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'domain_zone_form': domain_zone_form,
        })
        return c

class MailingListDomainCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = MailingListDomain 
    fields = [ 'member', 'label', 'domain_zone'  ]
    template_name = 'mailing_list_domain_form.html'

    def get_form(self):
        form = super().get_form()
        form.fields['domain_zone'].queryset = DomainZone.objects.filter(member_id=self.kwargs['member_pk']).order_by('domain')
        return form

    def get_success_url(self):
        return reverse_lazy('mailing-list-domain-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'domain_zone_form': domain_zone_form,
        })
        return c

class MailingListDomainDeleteView(MemberResourceMixin, DeleteView):
    model = MailingListDomain 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('mailing-list-domain-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MailingListDomainDisableView(DisableMixin):
    model = MailingListDomain

    def get_success_url(self):
      return reverse_lazy('mailing-list-domain-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MailingListDomainEnableView(EnableMixin):
    model = MailingListDomain 
        
    def get_success_url(self):
        return reverse_lazy('mailing-list-domain-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )


class MailingListListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = MailingList 
    context_object_name = 'entity_list'
    template_name = 'mailing_list_list.html'

class MailingListUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = MailingList 
    template_name = 'mailing_list_form.html'
    fields = [ 'member', 'listname', 'mailing_list_domain'  ]

    def get_form(self):
        form = super().get_form()
        form.fields['mailing_list_domain'].queryset = MailingListDomain.objects.filter(member_id=self.kwargs['member_pk'])
        return form

    def get_success_url(self):
        return reverse_lazy('mailing-list-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        mailing_list_domain_view = MailingListDomainCreateView()
        mailing_list_domain_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        mailing_list_domain_view.request = self.request
        mailing_list_domain_form = mailing_list_domain_view.get_form()

        c.update({ 
            'mailing_list_domain_form': mailing_list_domain_form,
        })
        return c

class MailingListCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = MailingList 
    fields = [ 'member', 'listname', 'mailing_list_domain'  ]
    template_name = 'mailing_list_form.html'

    def get_form(self):
        form = super().get_form()
        form.fields['mailing_list_domain'].queryset = MailingListDomain.objects.filter(member_id=self.kwargs['member_pk'])
        return form

    def get_success_url(self):
        return reverse_lazy('mailing-list-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        mailing_list_domain_view = MailingListDomainCreateView()
        mailing_list_domain_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        mailing_list_domain_view.request = self.request
        mailing_list_domain_form = mailing_list_domain_view.get_form()

        c.update({ 
            'mailing_list_domain_form': mailing_list_domain_form,
        })
        return c

class MailingListDeleteView(MemberResourceMixin, DeleteView):
    model = MailingList 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('mailing-list-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MailingListDisableView(DisableMixin):
    model = MailingList 

    def get_success_url(self):
      return reverse_lazy('mailing-list-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MailingListEnableView(EnableMixin):
    model = MailingList 
        
    def get_success_url(self):
        return reverse_lazy('mailing-list-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )


class ContactListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = Contact 
    context_object_name = 'entity_list'
    template_name = 'contact_list.html'
    order_by = 'first_name'

class ContactUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = Contact 
    template_name = 'contact_form.html'
    fields = [ 'member', 'first_name', 'last_name', 'email', 'is_billing'  ]

    def get_success_url(self):
        return reverse_lazy('contact-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class ContactCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = Contact 
    fields = [ 'member', 'first_name', 'last_name', 'email', 'is_billing'  ]
    template_name = 'contact_form.html'

    def get_success_url(self):
        return reverse_lazy('contact-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class ContactDeleteView(MemberResourceMixin, DeleteView):
    model = Contact 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('contact-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class ContactDisableView(DisableMixin):
    model = Contact 

    def get_success_url(self):
      return reverse_lazy('contact-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class ContactEnableView(EnableMixin):
    model = Contact 
        
    def get_success_url(self):
        return reverse_lazy('contact-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )


class WebsiteListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = Website 
    context_object_name = 'entity_list'
    template_name = 'website_list.html'
    order_by = 'label'

class WebsiteUpdateView(MemberResourceMixin, MemberResourceFormMixin, WebsiteFormMixin, UpdateView):
    model = Website 
    template_name = 'website_form.html'

    def form_valid(self, form):
        if not Authz.is_superuser():
            user_website_store = form.cleaned_data.get('website_store')
            if user_website_store:
                if not self.get_object().website_store == user_website_store:
                    self.success_message = "" 
                    form.add_error('website_store', _("Only super users can change the website_store."))
                    return self.form_invalid(form)

            user_partition = form.cleaned_data.get('partition')
            if user_partition:
                if not self.get_object().partition == user_partition:
                    self.success_message = "" 
                    form.add_error('partition', _("Only super users can change the partition."))
                    return self.form_invalid(form)

        return super().form_valid(form)
    
class WebsiteCreateView(MemberResourceMixin, MemberResourceFormMixin, WebsiteFormMixin, CreateView):
    model = Website 
    template_name = 'website_form.html'

    def form_valid(self, form):
        if not Authz.is_superuser():
            if form.cleaned_data.get('website_store'):
                self.success_message = "" 
                form.add_error('website_store', _("Only super users can set the website_store."))
                return self.form_invalid(form)
            if form.cleaned_data.get('partition'):
                self.success_message = "" 
                form.add_error('partition', _("Only super users can set the partition."))
                return self.form_invalid(form)

        return super().form_valid(form)

class WebsiteDeleteView(MemberResourceMixin, DeleteView):
    model = Website 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('websites-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class WebsiteDisableView(DisableMixin):
    model = Website 

    def get_success_url(self):
      return reverse_lazy('websites-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class WebsiteEnableView(EnableMixin):
    model = Website 
        
    def get_success_url(self):
        return reverse_lazy('websites-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )


class EmailAliasUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = EmailAlias 
    fields = [ 'member', 'local', 'domain_name', 'target' ]
    template_name = 'email_alias_form.html'

    def get_form(self):
        form = super().get_form()

        # Before overriding the widget, grap the help text.
        help_text = form.fields['domain_name'].help_text

        CHOICES = get_distinct_mx_record_choices(self.kwargs['member_pk']) 
        form.fields['domain_name'] = forms.ChoiceField(choices=CHOICES)
        ea = EmailAlias.objects.get(id=self.kwargs['pk'])
        form.fields['domain_name'].initial = ea.domain_name 
        form.fields['domain_name'].help_text = help_text 
        return form

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'domain_zone_form': domain_zone_form,
        })
        return c

    def get_success_url(self):
        return reverse('email-list', args=[self.kwargs['member_pk']])

class EmailAliasCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = EmailAlias 
    fields = [ 'member', 'local', 'domain_name', 'target' ]
    template_name = 'email_alias_form.html'

    def get_form(self):
        form = super().get_form()
        # Before overriding the widget, grap the help text.
        help_text = form.fields['domain_name'].help_text

        CHOICES = get_distinct_mx_record_choices(self.kwargs['member_pk']) 
        form.fields['domain_name'] = forms.ChoiceField(choices=CHOICES)
        form.fields['domain_name'].help_text = help_text
        return form

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'domain_zone_form': domain_zone_form,
        })
        return c

    def get_success_url(self):
        return reverse('email-list', args=[self.kwargs['member_pk']])

class EmailAliasDeleteView(MemberResourceMixin, DeleteView):
    model = EmailAlias 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailAliasDisableView(DisableMixin):
    model = EmailAlias 

    def get_success_url(self):
      return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailAliasEnableView(EnableMixin):
    model = EmailAlias 
        
    def get_success_url(self):
        return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MysqlDatabaseListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = MysqlDatabase 
    context_object_name = 'entity_list'
    template_name = 'mysql_database_list.html'
    order_by = 'name'

class MysqlDatabaseUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = MysqlDatabase 
    fields = [ 'member', 'name' ]
    template_name = 'mysql_database_form.html'

    def get_success_url(self):
        return reverse('mysql-database-list', args=[self.kwargs['member_pk']])

class MysqlDatabaseCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = MysqlDatabase 
    fields = [ 'member', 'name' ]
    template_name = 'mysql_database_form.html'

    def get_success_url(self):
        return reverse('mysql-database-list', args=[self.kwargs['member_pk']])

class MysqlDatabaseDeleteView(MemberResourceMixin, DeleteView):
    model = MysqlDatabase 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('mysql-database-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MysqlDatabaseDisableView(DisableMixin):
    model = MysqlDatabase 

    def get_success_url(self):
      return reverse_lazy('mysql-database-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MysqlDatabaseEnableView(EnableMixin):
    model = MysqlDatabase 
        
    def get_success_url(self):
        return reverse_lazy('mysql-database-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class MysqlUserListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = MysqlUser 
    context_object_name = 'entity_list'
    template_name = 'mysql_user_list.html'
    order_by = 'user'

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        db = MysqlDatabase.objects.get(id=self.kwargs['pk'])

        c.update({ 
          'mysql_database_pk': db.id,
          'mysql_database': db.name
        })
        return c

class MysqlUserUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = MysqlUser 
    fields = [ 'member', 'user', 'password', 'mysql_database', 'permission' ]
    template_name = 'mysql_user_form.html'

    def get_success_url(self):
        return reverse('mysql-user-list', args=[self.kwargs['member_pk'], self.object.mysql_database.id])

class MysqlUserCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = MysqlUser 
    fields = [ 'member', 'user', 'password', 'mysql_database', 'permission' ]
    template_name = 'mysql_user_form.html'

    def get_form(self):
        form = super().get_form()
        form.fields['mysql_database'].initial = self.kwargs['mysql_database_pk']
        return form

    def get_success_url(self):
        return reverse('mysql-user-list', args=[ self.kwargs['member_pk'], self.object.mysql_database.id])

class MysqlUserDeleteView(MemberResourceMixin, DeleteView):
    model = MysqlUser 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('mysql-user-list', args=[self.kwargs['member_pk'], self.kwargs['mysql_database_pk']])

class MysqlUserDisableView(DisableMixin):
    model = MysqlUser 

    def get_success_url(self):
      return reverse_lazy('mysql-user-list', args = [ self.kwargs['member_pk'], self.kwargs['mysql_database_pk'] ] )

class MysqlUserEnableView(EnableMixin):
    model = MysqlUser 
        
    def get_success_url(self):
      return reverse_lazy('mysql-user-list', args = [ self.kwargs['member_pk'], self.kwargs['mysql_database_pk'] ] )

class AccessListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    model = Access 
    context_object_name = 'entity_list'
    template_name = 'access_list.html'
    order_by = 'login'

class AccessCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = Access 
    fields = [ 'member', 'login' ]
    template_name = 'access_form.html'

    def get_form(self):
        form = super().get_form()
        # Our end goal is to provide the user with a drop down list showing all
        # the logins they have access to AND also allow them to type in the complete
        # username of a login they don't have access to and grant that user access
        # to their membership.

        # The queryset is for validation, but choices ensures we don't
        # expose our entire membership's login info..
        queryset = Login.objects.all()

        # Before overriding, grab the help text
        help_text = form.fields['login'].help_text

        form.fields['login'] = forms.ModelChoiceField(queryset, help_text=help_text)
        form.fields['login'].choices = [] 
        return form

    def get_success_url(self):
      return reverse_lazy('access-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class AccessDeleteView(MemberResourceMixin, DeleteView):
    model = Access 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('access-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class AccessDisableView(DisableMixin):
    model = Access 

    def get_success_url(self):
      return reverse_lazy('access-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class AccessEnableView(EnableMixin):
    model = Access 
        
    def get_success_url(self):
        return reverse_lazy('access-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )


class EmailMailboxUpdateView(MemberResourceMixin, MemberResourceFormMixin, UpdateView):
    model = EmailMailbox 
    fields = [ 'member', 'local', 'domain_name', 'login' ]
    template_name = 'email_mailbox_form.html'
    def get_form(self):
        form = super().get_form()
        # Restrict logins to logins owned by this member.
        form.fields['login'].queryset = Login.objects.filter(member=self.kwargs['member_pk'])

        # Restrict domain names to a distinct list of MX records in this
        # members domain name list.

        # Before overriding the widget, grap the help text.
        help_text = form.fields['domain_name'].help_text

        CHOICES = get_distinct_mx_record_choices(self.kwargs['member_pk']) 
        form.fields['domain_name'] = forms.ChoiceField(choices=CHOICES)
        ed = EmailMailbox.objects.get(id=self.kwargs['pk'])
        form.fields['domain_name'].initial = ed.domain_name 
        form.fields['domain_name'].help_text = help_text
        return form

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        login_view = LoginCreateView()
        login_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        login_view.request = self.request
        login_form = login_view.get_form()

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'login_form': login_form,
            'domain_zone_form': domain_zone_form,
        })
        return c

    def get_success_url(self):
      return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailMailboxCreateView(MemberResourceMixin, MemberResourceFormMixin, CreateView):
    model = EmailMailbox 
    fields = [ 'member', 'local', 'domain_name', 'login' ]
    template_name = 'email_mailbox_form.html'

    def get_form(self):
        form = super().get_form()
        # Restrict logins to logins in this members account
        form.fields['login'].queryset = Login.objects.filter(member=self.kwargs['member_pk'])

        # Restrict domain names to a distinct list of MX records in this
        # members domain name list.

        # Before overriding the widget, grap the help text.
        help_text = form.fields['domain_name'].help_text
        CHOICES = get_distinct_mx_record_choices(self.kwargs['member_pk']) 
        form.fields['domain_name'] = forms.ChoiceField(choices=CHOICES)
        form.fields['domain_name'].help_text = help_text 
        return form

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        login_view = LoginCreateView()
        login_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        login_view.request = self.request
        login_form = login_view.get_form()

        domain_zone_view = DomainZoneCreateView()
        domain_zone_view.kwargs = { 'member_pk': self.kwargs['member_pk'] }
        domain_zone_view.request = self.request
        domain_zone_form = domain_zone_view.get_form()

        c.update({ 
            'login_form': login_form,
            'domain_zone_form': domain_zone_form,
        })
        return c

    def get_success_url(self):
      return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailMailboxDeleteView(MemberResourceMixin, DeleteView):
    model = EmailMailbox 
    context_object_name = 'entity';
    template_name = 'resource_confirm_delete.html'

    def get_success_url(self):
      return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailMailboxDisableView(DisableMixin):
    model = EmailMailbox 

    def get_success_url(self):
      return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailMailboxEnableView(EnableMixin):
    model = EmailMailbox 
        
    def get_success_url(self):
        return reverse_lazy('email-list', kwargs = { 'member_pk': self.kwargs['member_pk'] } )

class EmailListView(MemberResourceMixin, MemberResourceListMixin, ListView):
    """
    This view shows both email mailboxes and email aliases together.
    """
    model = EmailMailbox 
    context_object_name = 'entity_list'
    template_name = 'email_list.html'

    def get_queryset(self):
        mailboxes = EmailMailbox.objects.filter(member=self.kwargs['member_pk']).order_by(self.order_by)
        aliases = EmailAlias.objects.filter(member=self.kwargs['member_pk']).order_by(self.order_by)
        results = []
        for mailbox in mailboxes:
            results.append(mailbox)
        for alias in aliases:
            results.append(alias)
        return results

# Api Views are below.

#
# The first set of views are used when a petal communicates back to the hub
# to either verify the nonce or consume the nonce.
#

# The petal will send an API request with a text resource_class name.
# We need to verify the name and return a class object for the resource's
# runner.
def get_runner_object_for_resource_class(resource_class):
    if resource_class == "Login":
        return LoginRunner
    elif resource_class == "DomainZone":
        return DomainZoneRunner
    elif resource_class == "EmailMailbox":
        return EmailMailboxRunner
    elif resource_class == "Website":
        return WebsiteRunner
    elif resource_class == "EmailAlias":
        return EmailAliasRunner

    shared.log_error("Failed to find runner for {0}".format(resource_class))

    return None

def verify_nonce(nonce = None, resource_class = None, minutes = 1, ):
    # Look for the nonce.
    queryObject = None
    time_cutoff = timezone.now()-timezone.timedelta(minutes=minutes)

    runner_object = get_runner_object_for_resource_class(resource_class)    

    if runner_object: 
        if runner_object.objects.filter(nonce=nonce, nonce_created__gt=time_cutoff, nonce_consumed=None).exists():
            return True
        else:
            shared.log_error("Couldn't find nonce {1} with cutoff {0}".format(time_cutoff, nonce))
    return False

"""
Each petal will send back each nonce it receives for confirmation that it is
valid. This view should lookup the nonce and make sure it is still valid.
We simply return an empty 200 code if it's valid or a 404 if it's not.
"""
@csrf_exempt
def api_verify_nonce(httpRequest):
    if httpRequest.method == 'POST':
        try:
            resource_class = httpRequest.POST.get('resource_class')
            nonce = httpRequest.POST.get('nonce')
        except KeyError:
            return HttpResponseNotFound()

        if verify_nonce(nonce, resource_class):
            return HttpResponse()
    else:
        shared.log_error("Verified nonce receved GET request, ignoring.")
    return HttpResponseNotFound()

@csrf_exempt
def api_update_resource_status(httpRequest):
    if httpRequest.method == 'POST':
        try:
            resource_class = httpRequest.POST.get('resource_class')
            nonce = httpRequest.POST.get('nonce')
            error = httpRequest.POST.get('error')
        except KeyError:
            return HttpResponseNotFound()

        # Sometimes it takes a while to create a resource, so let's allow the nonce to be
        # up to 10 minutes old.
        minutes = 10 
        if verify_nonce(nonce, resource_class, minutes = minutes):
            # We have to retrieve the actual runner so we can set the current_status to the
            # desired status.
            runner_object = get_runner_object_for_resource_class(resource_class)    
            runner = runner_object.objects.get(nonce=nonce)
            # Errors trump all. This means something went wrong. Only update the error field with the error.
            # We leave the status as PENDING_INPROGRESS.
            if error:
                runner_object.objects.filter(nonce=nonce).update(error=error, nonce_consumed=timezone.now())
                shared.log_error("Reporting a runner error: {0}.".format(error))
            # Deleted items should have their status updated AND de-activated because they are no
            # longer needed.
            elif runner_object.desired_status == RunnerCommon.STATUS_DELETED:
                runner_object.objects.filter(nonce=nonce).update(current_status=runner.desired_status, is_active=False, nonce_consumed=timezone.now())
                shared.log_debug("Runner deletion complete with nonce {0}".format(nonce))
            # Everyone else: just update the status and we are done.
            else:
                runner_object.objects.filter(nonce=nonce).update(current_status=runner.desired_status, nonce_consumed=timezone.now())
                shared.log_debug("Runner update complete with nonce {0}".format(nonce))
            return HttpResponse()

    return HttpResponseNotFound()

#
# The next set of views are used by the rest framework.
#

# Create a special Mixin for user authentication. Since the rest framework
# does not use the standard Django auth setup, our middleware doesn't catch
# the username.
class ApiAuthMixin():
    def perform_authentication(self, request):
        super().perform_authentication(request)
        Authz.init(request.user)

# Special case for member object.
class ApiMemberViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    serializer_class = MemberSerializer

    def get_queryset(self):
        # Restrict to memberships you have access to.
        return Authz.get_permissioned_queryset(Member)

# Special case for the many to many join fields uniting websites to logins and websites
# to domain names. Only create is available due to the mixins used.
class ApiWebsiteLoginViewSet(ApiAuthMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    API endpoint to create a mapping record between a web site and the logins that should have access to that web site.
    """

    serializer_class = WebsiteLoginSerializer

class ApiWebsiteDomainNameViewSet(ApiAuthMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    API endpoint to create a mapping record between a web site and the domain name that should direct traffic to that web site.
    """

    serializer_class = WebsiteDomainNameSerializer

class ApiMysqlDatabaseViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete MySQL databases.
    """

    serializer_class = MysqlDatabaseSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all MySQL databases owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(MysqlDatabase, member_id = member)
        serializer = MysqlDatabaseSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(MysqlDatabase)
        return queryset

class ApiMysqlUserViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete MySQL users.
    """

    serializer_class = MysqlUserSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all MySQL users owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(MysqlUser, member_id = member)
        serializer = MysqlUserSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(MysqlUser)
        return queryset

class ApiWebsiteViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to create, edit, or delete web sites.
    """

    serializer_class = WebsiteSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all web sites owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(Website, member_id = member)

        serializer = WebsiteSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(Website)
        return queryset

class ApiLoginViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete logins.
    """

    serializer_class = LoginSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all logins owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(Login, member_id = member)

        serializer = LoginSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(Login)

        return queryset

class ApiAccessViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete membership access records.
    """

    serializer_class = AccessSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all access records owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(Access, member_id = member)

        serializer = AccessSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(Access)

        return queryset

class ApiEmailAliasViewSet(ApiAuthMixin, viewsets.ModelViewSet,):
    """
    API endpoint to view, create, edit or delete email aliases.
    """
    serializer_class = EmailAliasSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all email aliases owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(EmailAlias, member_id = member)

        serializer = EmailAliasSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(EmailAlias)
        return queryset

class ApiEmailMailboxViewSet(ApiAuthMixin, viewsets.ModelViewSet,):
    """
    API endpoint to view, create, edit or delete email mailboxes.
    """
    serializer_class = EmailMailboxSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all email mailboxes owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(EmailMailbox, member_id = member)

        serializer = EmailMailboxSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(EmailMailbox)
        return queryset

class ApiMailingListDomainViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete mailing list domains.
    """
    serializer_class = MailingListDomainSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all mailing list domains owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(MailingListDomain, member_id = member)

        serializer = MailingListDomainSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(MailingListDomain)
        return queryset

class ApiDomainNameViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete domain names.
    """
    serializer_class = DomainNameSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all domain names owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(DomainName, member_id = member)

        serializer = DomainNameSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(DomainName)
        return queryset

class ApiDomainZoneViewSet(ApiAuthMixin, viewsets.ModelViewSet):
    """
    API endpoint to view, create, edit or delete domain zones.
    """

    serializer_class = DomainZoneSerializer

    @action(detail=False, url_path='mlist/(?P<member>\d+)')
    def mlist(self, request, member):
        """
        List all domain zones owned by the provided member id.
        """
        queryset = Authz.get_permissioned_queryset(DomainZone, member_id = member)

        serializer = DomainZoneSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Authz.get_permissioned_queryset(DomainZone)
        return queryset

# AJAX views are below.
def ajax_mx_options(request, member_id):
   
    """
    List unique MX domain names for the given member id - only returning the fully qualified string domain name as
    both key and value. The results are used to populate drop down options for creating email addresses.
    """
    # Make sure they have access to this member id
    if not Authz.has_member_access(member_id):
        return HttpResponseForbidden()

    # We already have a function for generating tuples for the UI form
    # (each value in the tuple is the same, e.g. (domain, domain).
    domain_tuples = get_distinct_mx_record_choices(member_id)

    # Convert tuples into something we can jsonify.
    domains = [] 
    for d1, d2 in domain_tuples:
        option = { 'value':  d1, 'text': d1 }
        domains.append(option)

    data = { "status" : 'success', 'data': domains }
    return JsonResponse(data)

def ajax_webproxy_options(request, member_id):
   
    """
    List unique WEBPROXY domain names for the given member id - returning the fully qualified string domain name as
    as value and id as the key. The results are used to populate drop down options for creating web sites.
    """
    # Make sure they have access to this member id
    if not Authz.has_member_access(member_id):
        return HttpResponseForbidden()

    # We already have a function for generating tuples for the UI form
    # (each value in the tuple is the same, e.g. (domain, domain).
    domain_tuples = get_distinct_webproxy_record_choices(member_id)

    # Convert tuples into something we can jsonify.
    domains = [] 
    for domain_id, domain in domain_tuples:
        option = { 'value':  domain_id, 'text': str(domain) }
        domains.append(option)

    data = { "status" : 'success', 'data': domains }
    return JsonResponse(data)

def ajax_mailing_list_domain_options(request, member_id):
   
    """
    List mailing_list domains for the given member id - returning the fully qualified string domain as
    as value and id as the key. The results are used to populate drop down options for creating mailing lists.
    """
    # Make sure they have access to this member id
    if not Authz.has_member_access(member_id):
        return HttpResponseForbidden()

    mailing_list_domains = []
    mlds = MailingListDomain.objects.filter(member=member_id).order_by('domain_zone__domain') 
    
    for mld in mlds:
        option = { 'value':  mld.id, 'text': str(mld) }
        mailing_list_domains.append(option)

    data = { "status" : 'success', 'data': mailing_list_domains }
    return JsonResponse(data)

def ajax_domain_zone_options(request, member_id):
   
    """
    List domain zones for the given member id - returning the fully qualified string domain zone as
    as value and id as the key. The results are used to populate drop down options for creating mailing list domains.
    """
    # Make sure they have access to this member id
    if not Authz.has_member_access(member_id):
        return HttpResponseForbidden()

    domain_zones = []
    dzs = DomainZone.objects.filter(member=member_id).order_by('domain') 
    
    for zone in dzs:
        option = { 'value':  zone.id, 'text': zone.domain }
        domain_zones.append(option)

    data = { "status" : 'success', 'data': domain_zones }
    return JsonResponse(data)

def ajax_generate_random_password(request):
    if not Authz.is_authenticated():
        # You have to be logged in, we aren't a global password generator
        return HttpResponseForbidden()

    data = { "password" : utils.get_random_password() }
    return JsonResponse(data)

def ajax_password_reset(request, login_id):
    if not Authz.is_authenticated():
        # You have to be logged in to reset a password this way. 
        return HttpResponseForbidden()

    login = get_object_or_404(Login, pk=login_id) 
    if not login.recovery_email:
        results = { "status": "fail", "data": { "login": _("That login does not have a recovery email.") } }
    else:
        password_reset = PasswordReset(login_id=login.id, notify=True)
        password_reset.save()
    results = { "status": "success" }
    return JsonResponse(results)

def ajax_select2_login_search(request):
    if not Authz.is_authenticated():
        # You have to be logged in (this is a little sensitive - it tests whether a given
        # username exists in our system.
        return HttpResponseForbidden()

    # q is the search term provided by the username. Must be an exact match for
    # a username.
    query_term = request.GET.get('q')

    # Member id helps us restrict access to logins for the given membership. 
    member_id = request.GET.get('member_id')

    # Cut them off if no member_id is provided or they don't have access to the member_id.
    if not member_id:
        return HttpResponseForbidden()

    if not Authz.has_member_access(member_id):
        return HttpResponseForbidden()

    # form tells us what form we are preparing this list for. Either websites (which means
    # restrict the search to users with shell access) or access, which means exclude
    # logins already assigned to this membership.
    form = request.GET.get('form')

    results = []
    # Pre-populate the results with all the usernames this username has access
    # to in the given membership. This includes ones without shell_access (they
    # will be granted shell access on save) and excludes disabled ones.
    logins = Authz.get_permissioned_queryset(Login, member_id=member_id)
    # Filter out disabled ones.
    logins = logins.filter(is_active=True)

    if form == 'access':
        # Further restrict - don't include users that already have access to this membership.
        logins = logins.exclude(access__login__isnull=False)
   
    if logins:
        for login in logins:
            results.append({ "id": login.id, "text": login.username })

    # If they are passing a query term, add in any exact matches for the username
    # they are searching for, even if they don't have access to this username. Only
    # usernames with shell_access can be found.
    if query_term:
        login = Login.objects.filter(username=query_term, is_active=True)
        if form == "shell":
            login = login.filter(shell_access=True)
        if login:
            results.append({"id": login[0].id, "text": query_term})

    data = { "results": results }
    return JsonResponse(data)

class AjaxLoginEmailMailboxCreateView(UserPassesTestMixin, ListView):
    """
    This view provides a list of all Email Mailboxs for the login_id passed
    as a parameter to the view.
    
    It is loaded via ajax on the login form so people can add/remove email
    addresses while editing their login.
    """

    model = EmailMailbox 
    fields = [ 'local', 'domain_name', 'login', 'member' ]
    template_name = 'ajax/login_email_mailbox_form.html'

    def test_func(self):
        # Only grant access if the login's member id is assigned to their
        # username in the Access table.
        login = Login.objects.get(pk = self.kwargs['login_id'])
        return Authz.has_member_access(login.member_id)

    def get_context_data(self, **kwargs):
        # Get the parent context, which includes the Email Mailbox form.
        c = super().get_context_data(**kwargs)

        # Create a list of all existing Email Mailboxs.
        login = Login.objects.get(pk = self.kwargs['login_id'])
        email_mailboxes =  EmailMailbox.objects.filter(login_id = login.id)

        c.update({ 
            'email_mailboxes': email_mailboxes,
        })
        return c


