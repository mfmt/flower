function Flower() {
  // Build a client.
  var auth = new coreapi.auth.SessionAuthentication({
    csrfCookieName: 'csrftoken',
    csrfHeaderName: 'X-CSRFToken'
  })
  var client = new coreapi.Client({auth: auth});
  
  // Ensure fields that need boostrap classes get bootstrap classes.
  bootstrap_field_classes();

  if ($('#flower-set-lang').length) {
    initialize_set_lang();
  }
  if ($('#flower-login-form-update').length) {
    // Handle Login update form
    initialize_login_password_field();
    initialize_login_update_form();
  }
  if ($('#flower-login-form-create').length) {
    // Handle Login create form
    initialize_login_password_field();
  }
  if ($('#flower-demo-form').length) {
    // Handle Login create form
    initialize_login_password_field();
  }

  if ($('#flower-website-form').length) {
    initialize_website_form();
  }
  if ($('#flower-modal-add-login').length) {
    // handle login form that is included via modal on other forms (like email box)
    initialize_modal_add_login();
    initialize_login_password_field();
  }
  if ($('#flower-domain-name-form').length) {
    initialize_domain_name_form();
  }
  if ($('#flower-mysql-user-form').length) {
    initialize_mysql_user_form();
  }
  if ($('#flower-modal-add-domain-zone').length) {
    initialize_modal_add_domain_zone();
  }
  if ($('#flower-modal-add-mailing-list-domain').length) {
    initialize_modal_add_mailing_list_domain();
  }

  function initialize_set_lang() {
    $('#flower-choose-lang').change(function() {
      $('#flower-set-lang').submit();
    });
  }
  function initialize_website_form() {
    function enable_website_app() {
      $('#flower-install-mysql-database').prop('checked', true);
      $('#div_id_website_app_email').show();
    }
    function disable_website_app() {
      $('#flower-install-mysql-database').prop('checked', false);
      $('#div_id_website_app_email').hide();
      $('#id_website_app_email').val('');
    }
    $('#id_website_app').change(function() {
      if($('#id_website_app').val() > 0) {
        enable_website_app(); 
      }
      else {
        disable_website_app(); 
      }
    });

    if($('#id_website_app').val() > 0) {
      enable_website_app();
    }
    else {
      disable_website_app();
    }
  }
  function hide_all_fields_except_category() {
    $('#div_id_ttl').hide();
    $('#flower-domain-name-label-and-zone').hide();
    $('#div_id_ip_address').hide();
    $('#div_id_server_name').hide();
    $('#div_id_txt').hide();
    $('#div_id_distance').hide();
    $('#div_id_port').hide();
    $('#div_id_weight').hide();
    $('#div_id_sshfp_type').hide();
    $('#div_id_sshfp_algorithm').hide();
    $('#div_id_sshfp_data').hide();
    $('#flower-change-category').hide();
    $('#flower-domain-name-save').hide();
    $('#flower-choose-category').show();

  }
  function initialize_domain_name_form() {
    // Check if the category value has been entered. If it's empty, it's a create form,
    // otherwise, it's an update form.
    var chosen_category_value = $('#id_category').val();

    // The map dict is set in the domain_name_form.html template
    var chosen_category = domain_name_category_map[chosen_category_value];

    // Set a friendly display in the template.
    $('#flower-chosen-category').html(chosen_category);

    if ( chosen_category_value == "") {
      // This is the brand new create form.
      hide_all_fields_except_category();

      $('#id_category').change(function() {
        init = true;
        chosen_category_value = $('#id_category').val();
        chosen_category = domain_name_category_map[chosen_category_value];
        show_hide_domain_name_fields(chosen_category, init)
      });
    }
    else {
      // This is an update field. 
      init = false;
      show_hide_domain_name_fields(chosen_category, init)
    }
  }

  function show_hide_domain_name_fields(chosen_category, init=false) {
    // Hide the infrequently used fields unless we need them.
    //
    // If init is true, we initialize certain fields with default
    // values. Otherwise, we leave the values alone.

    $('#flower-domain-name-save').show();

    if ( chosen_category == 'a') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ip_address').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').hide();
      if (init) $('#id_server_name').val('');
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'mx') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').show();
      if (init) $('#id_server_name').val('');
      $('#div_id_ip_address').hide();
      $('#div_id_txt').hide();
      $('#div_id_distance').show();
      if (init) $('#id_distance').val('10');
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'cname') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').show();
      $('#id_server_name').val('');
      $('#div_id_ip_address').hide();
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'webproxy') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').show();
      // webproxy_default_server_name is defined in templates/domain_name_form.html
      if (init) $('#id_server_name').val(webproxy_default_server_name);
      $('#div_id_ip_address').hide();
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'ptr') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').hide();
      if (init) $('#id_server_name').val('');
      $('#div_id_ip_address').show();
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'txt') {
      $('#div_id_ttl').show();
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_server_name').hide();
      if (init) $('#id_server_name').val('');
      $('#div_id_ip_address').hide();
      $('#div_id_txt').show();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'srv') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').show();
      if (init) $('#id_server_name').val('');
      $('#div_id_ip_address').hide();
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').show();
      $('#div_id_weight').show();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'aaaa') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').hide();
      if (init) $('#id_server_name').val('');
      $('#div_id_ip_address').show();
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').hide();
      $('#div_id_sshfp_type').hide();
      $('#div_id_sshfp_data').hide();
    }
    else if (chosen_category == 'sshfp') {
      $('#flower-domain-name-label-and-zone').show();
      $('#div_id_ttl').show();
      $('#div_id_server_name').hide();
      if (init) $('#id_server_name').val('');
      $('#div_id_ip_address').hide();
      $('#div_id_txt').hide();
      $('#div_id_distance').hide();
      if (init) $('#id_distance').val(0);
      $('#div_id_port').hide();
      $('#div_id_weight').hide();
      $('#div_id_sshfp_algorithm').show();
      $('#div_id_sshfp_type').show();
      $('#div_id_sshfp_data').show();
    }

  }
  function initialize_modal_add_mailing_list_domain() {
    $('#flower-modal-submit-add-mailing-list-domain').click(function(ev) {
      ev.preventDefault();
      // Clear any previous error messages.
      $('#errors_label').html("");
      $('#errors_domain_zone').html("");
      var member = parseInt($('#id_member').val());
      var label = $('#id_label').val();
      var domain_zone_id = $('#id_domain_zone').val();
      let action = ["mailing-list-domain", "create"];
      let params = {
        member: member, 
        label: label, 
        domain_zone: domain_zone_id,
      };
      client.action(schema, action, params).then(function(result) {
        // hide the modal
        $('#flower-modal-add-mailing-list-domain').hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').removeClass('modal-backdrop');

        // Set to empty
        $('#id_label').val('');
        $('#id_domain_zone').val('');

        chosen = result.id;
        console.log("chosen y,", result.id);
        populate_mailing_list_domain_options(chosen);

      }).catch(function (error) {
        for (var field_name in error.content) {
          $('#errors_' + field_name).append("<li>" + error.content[field_name] + "</li>");
        }
      });
    });
  }
  function initialize_modal_add_domain_zone() {
    $('#flower-modal-submit-add-domain-zone').click(function(ev) {
      ev.preventDefault();
      // Clear any previous error messages.
      $('#errors_zone').html("");
      var member = parseInt($('#id_member').val());
      var domain = $('#id_domain').val();
      let action = ["domain-zone", "create"];
      let params = {
        member: member, 
        domain: domain,
      };
      client.action(schema, action, params).then(function(result) {
        // hide the modal
        $('#flower-modal-add-domain-zone').hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').removeClass('modal-backdrop');

        // Set to empty
        $('#id_domain').val('');

        // And refresh the appropriate drop down list
        // Which form are we on?
        var emaild = $('#flower-email-mailbox-form').length;
        var emaila = $('#flower-email-alias-form').length;
        var login = $('#flower-login-form-update').length;
        var mld = $('#flower-mailing-list-domain-form').length;
        var website = $('#flower-website-form').length;

        if (emaild || emaila || login) {
          chosen = result.domain
          populate_mx_domain_name_options(chosen);    
        }
        else if (mld) {
          chosen = result.id
          populate_domain_zone_options(chosen);
        }
        else if (website) {
          populate_webproxy_domain_name_options();
        }
      }).catch(function (error) {
        for (var field_name in error.content) {
          $('#errors_' + field_name).append("<li>" + error.content[field_name] + "</li>");
        }
      });
    });
  }
  function initialize_modal_add_login() {
    $('#flower-modal-submit-add-login').click(function(ev) {
      ev.preventDefault();
      // Clear any previous error messages.
      $('#errors_username').html("");
      $('#errors_password').html("");
      $('#errors_recovery_email').html("");
      var member_pk = $('#id_member').val();
      var username = $('#id_username').val();
      var first_name = $('#id_first_name').val();
      var last_name = $('#id_last_name').val();
      var password = $('#id_password').val();
      var recovery_email = $('#id_recovery_email').val();
      var notify = $('#flower_send_password_reset').prop("checked")
      let action = ["login", "create"];
      if (notify && !recovery_email.length) {
          $('#errors_recovery_email').append("<li>" + flower_notify_needs_recovery_email + "</li>");
          return;
      }
      let params = {
        member: parseInt(member_pk),
        first_name: first_name,
        last_name: last_name,
        username: username,
        password: password,
        recovery_email: recovery_email,
      };
      client.action(schema, action, params).then(function(result) {
        created_login_id = result.id
        if (notify) {
          // Register the password reset and send it.
          $.ajax({
            url: "/ajax/password-reset/" + created_login_id,
          }).then(function() {
            alert(flower_recovery_email_sent);
          });
        }
        // Every time we add a new email mailbox, hide the modal 
        $('#flower-modal-add-login').hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').removeClass('modal-backdrop');

        // Reset the fields.
        $('#id_username').val('');
        $('#id_first_name').val('');
        $('#id_last_name').val('');
        $('#id_password').val('');
        $('#id_recovery_email').val('');
        $('#flower_send_password_reset').prop("checked", false)

        // And re-populate the login drop down list
        let action = ["login", "mlist"];
        let params = { 
          member: parseInt(member_pk) 
        };
        client.action(schema, action, params).then(function(list_result) {
          var select = $('select#id_login');
          // Empty the existing options.
          select.html("");
          for (var i = 0; i < list_result.length; i++) {
            value = list_result[i].id
            text = list_result[i].username;
            selected = false;
            if (value == created_login_id) {
              selected = true;
            }
            select.append(new Option(text, value, selected, selected))
          }
        }).catch(function (error) {
          console.log("Error listing logins.");
          console.log(error);
        });
      }).catch(function (error) {
        for (var field_name in error.content) {
          $('#errors_' + field_name).append("<li>" + error.content[field_name] + "</li>");
        }
      });
    });


  }
  
  // The rest are function definitions.
  function bootstrap_field_classes() {
    // Turn fields into select2 fields.
    
    // domain_names is used by the Web resource. 
    if ($('#flower-website-form').length) {
      $('#id_domain_names').select2({width: '100%'});

      var member_id = $('#id_member').val();
      // id_logins is used by the Web resource. It allows you to
      // choose multiple logins to access a web site.
      $('#id_logins').select2({
          width: '100%', 
          tags: true,
          ajax: {
            url: '/ajax/select2-login-search',
            dataType: 'json',
            delay: 250,
            data: function (params) {
              var query = {
                q: params.term,
                // Restrict to logins with shell access.
                form: 'shell',
                member_id: member_id
              }
              return query;
            },
          }
      });
    }

    if ($('#flower-access-form').length) {
      // id_login is used by the Access resource to designate
      // who should have access to your account.
      var member_id = $('#id_member').val();
      $('#id_login').select2({
          width: '100%', 
          tags: true,
          ajax: {
            url: '/ajax/select2-login-search',
            dataType: 'json',
            delay: 250,
            data: function (params) {
              var query = {
                q: params.term,
                // Restrict to logins with shell access.
                member_id: member_id,
                form: 'access',
              }
              return query;
            },
          },
      });
    }

    // Make our forms bootstrap forms by adding the required classes.
    $('input[type=text]').addClass('form-control');
    $('input[type=email]').addClass('form-control');
    $('input[type=number]').addClass('form-control');
    $('input[type=password]').addClass('form-control');
    $('select').addClass('form-control');
    $('textarea').addClass('form-control');
    $('input[type=checkbox]').addClass('form-check-input');
    $('input[type=radio]').addClass('form-check-input');
  }

  function populate_domain_zone_options(chosen = null) {
    var url = '/ajax/domain-zone-options/';
    var selector = 'select#id_domain_zone';
    populate_options(url, selector, chosen);
  }
  function populate_mx_domain_name_options(chosen = null) {
    var url = '/ajax/mx-options/';
    var selector = 'select#id_domain_name';
    populate_options(url, selector, chosen);
  }

  function populate_webproxy_domain_name_options() {
    var url = "/ajax/webproxy-options/"
    var selector = 'select#id_domain_names';
    populate_options(url, selector);
  }

  function populate_mailing_list_domain_options(chosen) {
    var url = "/ajax/mailing-list-domain-options/"
    var selector = 'select#id_mailing_list_domain';
    console.log("chosen x ", chosen);
    populate_options(url, selector, chosen);
  }
  // Re-populate an option group after an API call has created new options.
  //
  function populate_options(url, selector, chosen = null) {
    console.log("chosen z ", chosen);
    var member_id = $('#id_member').val();
    $.ajax({
      url: url + member_id,
    })
    .done(function(result) {
      var select = $(selector);
      select.empty();
      for (var i = 0; i < result.data.length; i++) {
        selected = false;
        if (result.data[i].value == chosen) {
          selected = true;
        }
        var opt = new Option(result.data[i].text, result.data[i].value, selected, selected);
        select.append(opt);
      }
    });
  }

  // Fetch a fresh list of email mailboxes via AJAX.
  function login_email_mailbox_fetch() {
    // Retrieve form to add new email mailboxes and list existing
    // email mailboxes..
    // Extract the login_id from the URL.
    // pathname is: member/<member_pk>/login/<login_id>/update
    var login_id = window.location.pathname.split('/')[4];
    $.ajax({
      url: "/ajax/login-email-mailbox/" + login_id,
    })
    .done(function(html) {
      $('#flower-mailbox-email').html(html);
      var member_pk = $('#id_member').val();

      bootstrap_field_classes();

      populate_mx_domain_name_options();    

      // Setup delete button provided by delete confirm modal.
      $('.flower-delete').click(function() {
        let id = $(this).data("id");
        let resource = $(this).data("resource");
        let action = [ resource, "delete" ];
        let params = {
          id: $(this).data("id"),
          member: member_pk,
        };
        client.action(schema, action, params).then(function(result) {
          // Refresh now that the email has been deleted.
          $('.modal-backdrop').remove();
          login_email_mailbox_fetch();
        });
      });

      // Setup add button for adding a new email mailbox.
      $('#flower-add-email-mailbox').click(function(ev) {
        ev.preventDefault();
        // Clear any previous error messages.
        $('#errors_local').html("");
        $('#errors_domain_name').html("");
        var domain_name = $('#id_domain_name').val();
        var local = $('#id_local').val();
        var login_id = $('#id_login').val();
        let action = ["email-mailbox", "create"];
        member = parseInt(member_pk);
        let params = {
          member: member, 
          domain_name: domain_name,
          local: local,
          login: parseInt(login_id),
        };
        client.action(schema, action, params).then(function(result) {
          // Every time we add a new email mailbox, refresh again to
          // get the latest list.
          login_email_mailbox_fetch();
        }).catch(function (error) {
          console.log(error);
          for (var field_name in error.content) {
            $('#errors_' + field_name).append("<li>" + error.content[field_name] + "</li>");
          }
        });
      });
    });
  }

  function initialize_login_password_field() {
    // Show passsword when you click in the field, but obscure it when
    // you exit the field.
    $('#id_password').blur(function() {
        $('#id_password').attr('type', 'password');
    });
    $('#id_password').focus(function(){
      $('#id_password').attr('type', 'text');
    });

    // If the form has a password value in it when the page is loaded,
    // be sure to obscure it
    if ($('#id_password').val()) {
        $('#id_password').attr('type', 'password');
    }
    // Setup link to generate random password.
    $('#flower-generate-random-password').click(function(ev) {
      ev.preventDefault();
      $.ajax({
        url: "/ajax/generate-random-password"
      })
      .done(function(data) {
        $('#id_password').val(data.password);
        // Change to text so they can see what they created.
        $('#id_password').attr('type', 'text');
        // Give it focus so when they click somewhere else is is obscured again.
        $('#id_password').focus();
      });
    });
  }

  function initialize_login_update_form() {
    // Retrieve a current copy of the email mailboxes and form when email settings are clicked.
    $("#flower-login-activate-email-settings").click(function() {
      login_email_mailbox_fetch();
    });
    
    // If shell access is enabled, show the ssh key field. If not, hide it.
    // Initial state:
    if (!$("#id_shell_access").is(':checked')) {
      $('#div_id_ssh_key').hide();
    }

    // Change on click:
    $('#id_shell_access').click(function() {
      if ($("#id_shell_access").is(':checked')) {
        $('#div_id_ssh_key').show();
      }
      else {
        $('#div_id_ssh_key').hide();
      }
    });

    // If nextcloud access is enabled, show the nextcloud quota field. If not, hide it.
    // Initial state:
    if (!$("#id_nextcloud_access").is(':checked')) {
      $('#div_id_nextcloud_quota').hide();
    }

    // Change on click:
    $('#id_nextcloud_access').click(function() {
      if ($("#id_nextcloud_access").is(':checked')) {
        $('#div_id_nextcloud_quota').show();
      }
      else {
        $('#div_id_nextcloud_quota').hide();
      }
    });
  }
  function initialize_mysql_user_form() {
    // We have two password fields. One is the real one which is hidden in the form
    // and displays the mysql hashed version of the password on edit (we don't want
    // to confuse the user by showing a password that is not the plain text password).
    // The other password field is the fako display one that shows the random password
    // if you generate it. The code below tries to keep them in sync.

    // On initial, set random password.
    if($('#id_password').val() == "") {
      set_random_mysql_user_password();
    }

    $('#flower-mysql-user-generate-password').click(function(ev) {
      ev.preventDefault();
      set_random_mysql_user_password();
      
    });
    $('#flower-mysql-user-copy-password-to-clipboard').click(function(ev) {
      // The fako password field is disabled to users don't try to manually
      // change it. But since it is disabled, it can't be selected and copied.
      // This code enables it, copies the password and then disables it again.
      ev.preventDefault();
      var el = document.getElementById("flower-password-display");
      el.disabled = false;
      el.focus();
      el.select();
      document.execCommand("copy");
      el.disabled = true;
    });
  }

  function set_random_mysql_user_password() {
    $.ajax({
      url: "/ajax/generate-random-password"
    })
    .done(function(data) {
      // Set both password fields to the new password.
      $('#id_password').val(data.password);
      $('#flower-password-display').val(data.password);
    });
  }
}

$(document).ready(function() {
  Flower();
});


