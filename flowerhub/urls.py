from django.urls import path
from django.urls import include
from . import views
from .views import ajax_generate_random_password, ajax_select2_login_search, ajax_password_reset, ajax_mx_options, ajax_webproxy_options, ajax_domain_zone_options, ajax_mailing_list_domain_options
from django.views.generic import RedirectView
from django.contrib.auth.views import LoginView
from rest_framework import routers
from rest_framework.schemas import get_schema_view
from rest_framework.documentation import include_docs_urls
from django.views.generic import TemplateView

# We put all of our API routes here.
router = routers.DefaultRouter()
router.register(r'member', views.ApiMemberViewSet, basename = 'Member')

router.register(r'domain-zone', views.ApiDomainZoneViewSet, basename = 'DomainZone')
router.register(r'domain-name', views.ApiDomainNameViewSet, basename = 'DomainName')
router.register(r'login', views.ApiLoginViewSet, basename = 'Login')
router.register(r'access', views.ApiAccessViewSet, basename = 'Access')
router.register(r'email-mailbox', views.ApiEmailMailboxViewSet, basename = 'EmailMailbox')
router.register(r'email-alias', views.ApiEmailAliasViewSet, basename = 'EmailAlias')
router.register(r'mailing-list-domain', views.ApiMailingListDomainViewSet, basename = 'MailingListDomain')
router.register(r'website', views.ApiWebsiteViewSet, basename = 'Website')
router.register(r'website-login', views.ApiWebsiteLoginViewSet, basename = 'WebsiteLogin')
router.register(r'website-domain-name', views.ApiWebsiteDomainNameViewSet, basename = 'WebsiteDomainName')
router.register(r'mysql-database', views.ApiMysqlDatabaseViewSet, basename = 'MysqlDatabase')
router.register(r'mysql-user', views.ApiMysqlUserViewSet, basename = 'MysqlUser')

# Used to auto create coreapi schema
urlpatterns = [
    # For language switching.
    path('i18n/', include('django.conf.urls.i18n')),

    # This url provides the built-in django login/logout urls
    path('accounts/', include('django.contrib.auth.urls')),

    # All api requests start with ^/api
    path('api/docs/', include_docs_urls(title='Flower API service')),
    path('api/schema/', get_schema_view(title='Pastebin API'), name='openapi-schema'),
    path('api/verify-nonce', views.api_verify_nonce),
    path('api/update-resource-status', views.api_update_resource_status),
    path('api/', include(router.urls)),

    # By providing api-auth, we provide the login/account links when viewing
    # the API via a browser - makes it easier to learn the API.
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # AJAX related views. These are internal api calls used by the javascript UI.
    # These should not be used external and are subject to change.
    path('ajax/login-email-mailbox/<int:login_id>', views.AjaxLoginEmailMailboxCreateView.as_view(), name='ajax-login-email-mailbox'),
    path('ajax/generate-random-password', ajax_generate_random_password, name='ajax-generate-random-password'),
    # Used by select2 plugin, expects query: ?q=username
    path('ajax/select2-login-search', ajax_select2_login_search, name='ajax-select2-login-search'),
    # Used by the ajax add login form.
    path('ajax/password-reset/<int:login_id>', ajax_password_reset, name='ajax-password-reset'),

    # Used to populate drop downs
    path('ajax/mx-options/<int:member_id>', ajax_mx_options, name='ajax-mx-options'),
    path('ajax/webproxy-options/<int:member_id>', ajax_webproxy_options, name='ajax-webproxy-options'),
    path('ajax/domain-zone-options/<int:member_id>', ajax_domain_zone_options, name='ajax-domain_zone-options'),
    path('ajax/mailing-list-domain-options/<int:member_id>', ajax_mailing_list_domain_options, name='ajax-mailing-list-domain-options'),

    # By default, show the home page, which is a redirect view depending
    # on whether you are logged in (get member-list) or not (get welcome page).
    path('', views.HomeView.as_view(), name='home'),

    # Display a welcome page specifically for non-logged in users.
    path('welcome', views.WelcomeView.as_view(), name='welcome'),

    # If we are in demo mode, this view allows users to create a membership and login.
    path('demo', views.DemoView.as_view(), name='demo'),

    # The feedback model is a bit different - only super users can read and edit, but
    # all users can submit. Also, not related to membership or user at all.
    path('feedback', views.FeedbackListView.as_view(), name='feedback-list'),
    path('feedback/create', views.FeedbackCreateView.as_view(), name='feedback-create'),
    path('feedback/<int:pk>/delete', views.FeedbackDeleteView.as_view(), name='feedback-delete'),
    path('feedback/<int:pk>', views.FeedbackDetailView.as_view(), name='feedback-detail'),

    # Members. The member object is different from all other urls - we have differnet
    # permissions, etc. Also, there is no Member Detail - instead the member details
    # are displayed on all page at the top.
    path('member', views.MemberListView.as_view(), name='member-list'),
    path('member/<int:pk>', views.MemberDetailView.as_view(), name='member-detail'),
    path('member/<int:pk>/update', views.MemberUpdateView.as_view(), name='member-update'),
    path('member/create', views.MemberCreateView.as_view(), name='member-create'),
    path('member/<int:pk>/delete', views.MemberDeleteView.as_view(), name='member-delete'),
    path('member/<int:pk>/enable', views.MemberEnableView.as_view(), name='member-enable'),
    path('member/<int:pk>/disable', views.MemberDisableView.as_view(), name='member-disable'),

    # The petal and partition objects are different too - only accessible to super users.
    path('petal', views.PetalListView.as_view(), name='petal-list'),
    path('petal/<int:pk>/update', views.PetalUpdateView.as_view(), name='petal-update'),
    path('petal/create', views.PetalCreateView.as_view(), name='petal-create'),
    path('petal/<int:pk>/delete', views.PetalDeleteView.as_view(), name='petal-delete'),

    path('partition', views.PartitionListView.as_view(), name='partition-list'),
    path('partition/<int:pk>/update', views.PartitionUpdateView.as_view(), name='partition-update'),
    path('partition/create', views.PartitionCreateView.as_view(), name='partition-create'),
    path('partition/<int:pk>/delete', views.PartitionDeleteView.as_view(), name='partition-delete'),

    # Logins
    path('member/<int:member_pk>/login', views.LoginListView.as_view(), name='login-list'),
    path('member/<int:member_pk>/login/create', views.LoginCreateView.as_view(), name='login-create'),
    path('member/<int:member_pk>/login/<int:pk>/update', views.LoginUpdateView.as_view(), name='login-update'),
    path('member/<int:member_pk>/login/<int:pk>/delete', views.LoginDeleteView.as_view(), name='login-delete'),
    path('member/<int:member_pk>/login/<int:pk>/disable', views.LoginDisableView.as_view(), name='login-disable'),
    path('member/<int:member_pk>/login/<int:pk>/enable', views.LoginEnableView.as_view(), name='login-enable'),

    # Domain Zones
    # The detail view of domain zone displays a list of all domain names assigned to that zone.
    path('member/<int:member_pk>/domain-zone/<int:pk>', views.DomainNameListView.as_view(), name='domain-name-list'),
    path('member/<int:member_pk>/domain-zone', views.DomainZoneListView.as_view(), name='domain-zone-list'),
    path('member/<int:member_pk>/domain-zone/create', views.DomainZoneCreateView.as_view(), name='domain-zone-create'),
    path('member/<int:member_pk>/domain-zone/<int:pk>/delete', views.DomainZoneDeleteView.as_view(), name='domain-zone-delete'),
    path('member/<int:member_pk>/domain-zone/<int:pk>/disable', views.DomainZoneDisableView.as_view(), name='domain-zone-disable'),
    path('member/<int:member_pk>/domain-zone/<int:pk>/enable', views.DomainZoneEnableView.as_view(), name='domain-zone-enable'),

    # Domain Names
    path('member/<int:member_pk>/domain-name/create/<int:domain_zone_pk>', views.DomainNameCreateView.as_view(), name='domain-name-create'),
    path('member/<int:member_pk>/domain-name/<int:pk>/update', views.DomainNameUpdateView.as_view(), name='domain-name-update'),
    path('member/<int:member_pk>/domain-name/<int:pk>/delete/<int:domain_zone_pk>', views.DomainNameDeleteView.as_view(), name='domain-name-delete'),
    path('member/<int:member_pk>/domain-name/<int:pk>/disable/<int:domain_zone_pk>', views.DomainNameDisableView.as_view(), name='domain-name-disable'),
    path('member/<int:member_pk>/domain-name/<int:pk>/enable/<int:domain_zone_pk>', views.DomainNameEnableView.as_view(), name='domain-name-enable'),

    # Mailing List Domains
    path('member/<int:member_pk>/mailing-list-domain', views.MailingListDomainListView.as_view(), name='mailing-list-domain-list'),
    path('member/<int:member_pk>/mailing-list-domain/create', views.MailingListDomainCreateView.as_view(), name='mailing-list-domain-create'),
    path('member/<int:member_pk>/mailing-list-domain/<int:pk>/update', views.MailingListDomainUpdateView.as_view(), name='mailing-list-domain-update'),
    path('member/<int:member_pk>/mailing-list-domain/<int:pk>/delete', views.MailingListDomainDeleteView.as_view(), name='mailing-list-domain-delete'),
    path('member/<int:member_pk>/mailing-list-domain/<int:pk>/disable', views.MailingListDomainDisableView.as_view(), name='mailing-list-domain-disable'),
    path('member/<int:member_pk>/mailing-list-domain/<int:pk>/enable', views.MailingListDomainEnableView.as_view(), name='mailing-list-domain-enable'),

    # Mailing Lists
    path('member/<int:member_pk>/mailing-list', views.MailingListListView.as_view(), name='mailing-list-list'),
    path('member/<int:member_pk>/mailing-list/create', views.MailingListCreateView.as_view(), name='mailing-list-create'),
    path('member/<int:member_pk>/mailing-list/<int:pk>/update', views.MailingListUpdateView.as_view(), name='mailing-list-update'),
    path('member/<int:member_pk>/mailing-list/<int:pk>/delete', views.MailingListDeleteView.as_view(), name='mailing-list-delete'),
    path('member/<int:member_pk>/mailing-list/<int:pk>/disable', views.MailingListDisableView.as_view(), name='mailing-list-disable'),
    path('member/<int:member_pk>/mailing-list/<int:pk>/enable', views.MailingListEnableView.as_view(), name='mailing-list-enable'),

    # Contacts
    path('member/<int:member_pk>/contact', views.ContactListView.as_view(), name='contact-list'),
    path('member/<int:member_pk>/contact/create', views.ContactCreateView.as_view(), name='contact-create'),
    path('member/<int:member_pk>/contact/<int:pk>/update', views.ContactUpdateView.as_view(), name='contact-update'),
    path('member/<int:member_pk>/contact/<int:pk>/delete', views.ContactDeleteView.as_view(), name='contact-delete'),
    path('member/<int:member_pk>/contact/<int:pk>/disable', views.ContactDisableView.as_view(), name='contact-disable'),
    path('member/<int:member_pk>/contact/<int:pk>/enable', views.ContactEnableView.as_view(), name='contact-enable'),

    # Website
    path('member/<int:member_pk>/website', views.WebsiteListView.as_view(), name='website-list'),
    path('member/<int:member_pk>/website/create', views.WebsiteCreateView.as_view(), name='website-create'),
    path('member/<int:member_pk>/website/<int:pk>/update', views.WebsiteUpdateView.as_view(), name='website-update'),
    path('member/<int:member_pk>/website/<int:pk>/delete', views.WebsiteDeleteView.as_view(), name='website-delete'),
    path('member/<int:member_pk>/website/<int:pk>/disable', views.WebsiteDisableView.as_view(), name='website-disable'),
    path('member/<int:member_pk>/website/<int:pk>/enable', views.WebsiteEnableView.as_view(), name='website-enable'),

    # Email Alias. Use email-list for list view, shared with Email Alias.
    path('member/<int:member_pk>/email-alias/create', views.EmailAliasCreateView.as_view(), name='email-alias-create'),
    path('member/<int:member_pk>/email-alias/<int:pk>/update', views.EmailAliasUpdateView.as_view(), name='email-alias-update'),
    path('member/<int:member_pk>/email-alias/<int:pk>/delete', views.EmailAliasDeleteView.as_view(), name='email-alias-delete'),
    path('member/<int:member_pk>/email-alias/<int:pk>/disable', views.EmailAliasDisableView.as_view(), name='email-alias-disable'),
    path('member/<int:member_pk>/email-alias/<int:pk>/enable', views.EmailAliasEnableView.as_view(), name='email-alias-enable'),

    # MySQL Databases
    # The detail view of MySQL database displays a list of all users with access to that database.
    path('member/<int:member_pk>/mysql-database/<int:pk>', views.MysqlUserListView.as_view(), name='mysql-user-list'),
    path('member/<int:member_pk>/mysql-database', views.MysqlDatabaseListView.as_view(), name='mysql-database-list'),
    path('member/<int:member_pk>/mysql-database/create', views.MysqlDatabaseCreateView.as_view(), name='mysql-database-create'),
    path('member/<int:member_pk>/mysql-database/<int:pk>/delete', views.MysqlDatabaseDeleteView.as_view(), name='mysql-database-delete'),
    path('member/<int:member_pk>/mysql-database/<int:pk>/disable', views.MysqlDatabaseDisableView.as_view(), name='mysql-database-disable'),
    path('member/<int:member_pk>/mysql-database/<int:pk>/enable', views.MysqlDatabaseEnableView.as_view(), name='mysql-database-enable'),

    # MySQL Users
    path('member/<int:member_pk>/mysql-user/create/<int:mysql_database_pk>', views.MysqlUserCreateView.as_view(), name='mysql-user-create'),
    path('member/<int:member_pk>/mysql-user/<int:pk>/update', views.MysqlUserUpdateView.as_view(), name='mysql-user-update'),
    path('member/<int:member_pk>/mysql-user/<int:pk>/delete/<int:mysql_database_pk>', views.MysqlUserDeleteView.as_view(), name='mysql-user-delete'),
    path('member/<int:member_pk>/mysql-user/<int:pk>/disable/<int:mysql_database_pk>', views.MysqlUserDisableView.as_view(), name='mysql-user-disable'),
    path('member/<int:member_pk>/mysql-user/<int:pk>/enable/<int:mysql_database_pk>', views.MysqlUserEnableView.as_view(), name='mysql-user-enable'),

    # Access
    path('member/<int:member_pk>/access', views.AccessListView.as_view(), name='access-list'),
    path('member/<int:member_pk>/access/create', views.AccessCreateView.as_view(), name='access-create'),
    path('member/<int:member_pk>/access/<int:pk>/delete', views.AccessDeleteView.as_view(), name='access-delete'),
    path('member/<int:member_pk>/access/<int:pk>/disable', views.AccessDisableView.as_view(), name='access-disable'),
    path('member/<int:member_pk>/access/<int:pk>/enable', views.AccessEnableView.as_view(), name='access-enable'),

    # Email Mailbox. Use email-list for list view, shared with Email Alias.
    path('member/<int:member_pk>/email-mailbox/create', views.EmailMailboxCreateView.as_view(), name='email-mailbox-create'),
    path('member/<int:member_pk>/email-mailbox/<int:pk>/update', views.EmailMailboxUpdateView.as_view(), name='email-mailbox-update'),
    path('member/<int:member_pk>/email-mailbox/<int:pk>/delete', views.EmailMailboxDeleteView.as_view(), name='email-mailbox-delete'),
    path('member/<int:member_pk>/email-mailbox/<int:pk>/disable', views.EmailMailboxDisableView.as_view(), name='email-mailbox-disable'),
    path('member/<int:member_pk>/email-mailbox/<int:pk>/enable', views.EmailMailboxEnableView.as_view(), name='email-mailbox-enable'),

    # All Emails view (both email mailbox and email aliases
    path('member/<int:member_pk>/email', views.EmailListView.as_view(), name='email-list'),

]
