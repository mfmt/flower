""" 
This file contains all the code used to authorize a
user to access a given resource. It's a singleton.
"""

import threading

threadLocal = threading.local()

class Authz:
    # Initialize with a user
    def init(user=None):
        threadLocal.is_authenticated = False
        threadLocal.is_superuser = False
        threadLocal.username = None
        if user.is_authenticated:
            threadLocal.username = str(user)
            threadLocal.is_authenticated = True
            if user.is_superuser:
                threadLocal.is_superuser = True

    def get_username():
        return threadLocal.username

    def is_superuser():
        return threadLocal.is_superuser

    def is_authenticated():
        return threadLocal.is_authenticated

    # Check if the currently logged in user has member access to the membership
    # with the passed in primary key (pk).
    def has_member_access(pk):
        from flowerhub.models import Access
        if not Authz.is_authenticated():
            return False

        if Authz.is_superuser():
            return True

        return Access.objects.filter(member=pk, is_active=True, login__username=Authz.get_username()).exists()

    # Return a query set for the given object restriced to the records
    # accessible by the logged in user. We expect either a Member object or
    # any Member Resource object.
    def get_permissioned_queryset(obj, member_id = None):
        from flowerhub.models import Member
        if obj == Member and member_id:
            raise RuntimeError( _("Passing in a member_id is only allowed for Member resources, not member objects. Use has_member_access instead.") )

        # Superusers get everything.
        if Authz.is_superuser():
            if member_id:
                return obj.objects.filter(member=member_id)
            return obj.objects.all()

        # Special query for member objects.
        if obj == Member:
            return obj.objects.filter(access__login__username=Authz.get_username(), access__is_active=True)

        # If it's not a Member object we are looking for, we have to add a relationship to the query.
        if member_id:
            # Also restrict by the given member_id.
            return obj.objects.filter(member=member_id, member__access__login__username=Authz.get_username(), member__access__is_active=True)
        else:
            # Return all of them.
            return obj.objects.filter(member__access__login__username=Authz.get_username(), member__access__is_active=True)

