from django import forms
from flowerhub import models
from django.utils.translation import gettext_lazy as _

class DemoForm(forms.Form):
    membership_name = forms.CharField(label='Membership name', help_text=_("This is a demo, feel free to enter any made up name you want for this membership."), max_length=254)
    first_name = forms.CharField(label='First name', max_length=254)
    username = forms.CharField(label='Username', max_length=254, help_text=_("Please limit your username to lower case letters, numbers, periods, dashes and underscores."))
    password = forms.CharField(label='Password', max_length=254, help_text=_("Please enter a password that is at least 10 characters long."))
    domain = forms.CharField(label='Domain Name', max_length=254, help_text=_("Your domain name is the address you use online, like 'mayfirst.org' or 'people-link.net'. Since this is a demo, you can enter any domain name, even a made up one. A default one is provided for you."))

    def clean_membership_name(self):
        # Ensure unique.
        if models.Member.objects.filter(name = self.cleaned_data['membership_name']).exists():
            raise forms.ValidationError(_("That organization name is already taken in this demo"))
        return self.cleaned_data['membership_name']

    def clean_username(self):
        # Ensure valid
        models.valid_username(self.cleaned_data['username'])

        # Ensure unique
        if models.Login.objects.filter(username = self.cleaned_data['username']).exists():
            raise forms.ValidationError(_("That username is already taken in this demo"))

        return self.cleaned_data['username']

    def clean_password(self):
        # Ensure valid
        models.valid_password(self.cleaned_data['password'])
        return self.cleaned_data['password']

    def clean_domain(self):
        models.valid_domain_zone(self.cleaned_data['domain'])
        if models.DomainZone.objects.filter(domain = self.cleaned_data['domain']).exists():
            raise forms.ValidationError(_("That domain name is already taken in this demo"))
        return self.cleaned_data['domain']

