""" 
Utility functions used by the UI.
"""

from flowerhub import shared
import string
import os
from hashlib import sha1

def get_random_url_friendly_code():
    """
    It can't be too long (or it will wrap in an email) and can't have
    punctuation or the email client might not properly make it all 
    clickable.
    """
    chars = string.ascii_letters + string.digits
    return get_random_string(15, chars)

def get_random_password(length = 25):
    return get_random_string(length)

def get_random_string(length = 25, chars=None):
    if not chars:
        # Set default
        chars = string.ascii_letters + string.digits + '@!-/%^+{}#!&*()_'
    ret = ""
    while len(ret) < length:
        ret += chars[ord(os.urandom(1)) % len(chars)]
    return ret 

def convert_to_mysql_password_hash(plain):
    return '*' + sha1(sha1(plain.encode('utf-8')).digest()).hexdigest()


