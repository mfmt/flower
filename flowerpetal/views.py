from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseForbidden, HttpResponse
from flower.settings import FLOWER_PETAL_API_KEY_HASH
from flowerhub.shared import log_error, log_info, log_debug
import time
import hashlib
from .resourcer import resource_factory

@csrf_exempt
def process_resource(request):
    if request.method == "POST":
        try:
            log_debug("Received POST request")
            # Validate API key.
            api_key = request.POST.get('api_key')
            nonce = request.POST.get('nonce')
            resource_class = request.POST.get('resource_class')
            petal_action = int(request.POST.get('petal_action'))
            data = request.POST.get('data')
            if FLOWER_PETAL_API_KEY_HASH != hashlib.sha256(api_key.encode('utf-8')).hexdigest():
                log_debug("Failed to validate secret, shared key.")
                return HttpResponseForbidden()
        
            # Create a resourer class to create/edit/delete the resource.
            log_debug("Getting class for {0}".format(petal_action))
            r = resource_factory(petal_action)
            if r:
                r.initialize(resource_class, nonce, data)
            else:
                log_error("Failed to get resource class from factory for resource_class '{0}' and petal action '{1}'.".format(resource_class, petal_action))
        except Exception as e:
            log_error("Something went wrong for resource class '{0}' and petal action '{1}'. Error: {2}".format(resource_class, petal_action, e))

        return HttpResponse() 
    else:
        log_debug("Received non POST request")
        return HttpResponseForbidden()
