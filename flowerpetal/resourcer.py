from flowerhub import shared 
from flowerhub.shared import log_info, log_error, log_debug
import threading
from flower.settings import FLOWER_BASE_URL, FLOWER_MODE, FLOWER_MODE_DEV, FLOWER_MODE_TEST, BASE_DIR
import requests
import time
import json
import subprocess
import tempfile
import os
from django.template import Context
from django.template.loader import get_template
from pathlib import Path
import re

def resource_factory(petal_action):
    log_debug("Resource factory received petal_action {0}".format(petal_action))

    if petal_action == shared.PETAL_ACTION_NOOP:
        return NoOp()
    elif petal_action == shared.PETAL_ACTION_CREATE_LDAP_LOGIN:
        return CreateLdapLogin()
    elif petal_action == shared.PETAL_ACTION_DELETE_LDAP_LOGIN:
        return DeleteLdapLogin()
    elif petal_action == shared.PETAL_ACTION_DISABLE_LDAP_LOGIN:
        return DisableLdapLogin()
    elif petal_action == shared.PETAL_ACTION_ADD_EMAIL_ADDRESS:
        return AddEmailAddress()
    elif petal_action == shared.PETAL_ACTION_REMOVE_LAST_EMAIL_ADDRESS:
        return RemoveLastEmailAddress()
    elif petal_action == shared.PETAL_ACTION_REMOVE_EMAIL_ADDRESS:
        return RemoveEmailAddress()
    elif petal_action == shared.PETAL_ACTION_CREATE_DOMAIN_ZONE:
        return CreateDomainZone()
    elif petal_action == shared.PETAL_ACTION_DELETE_DOMAIN_ZONE:
        return DeleteDomainZone()
    elif petal_action == shared.PETAL_ACTION_CREATE_MAILDIR:
        return CreateMailDir()
    elif petal_action == shared.PETAL_ACTION_DELETE_MAILDIR:
        return DeleteMailDir()
    elif petal_action == shared.PETAL_ACTION_ADD_EMAIL_DOMAIN:
        return AddEmailDomain()
    elif petal_action == shared.PETAL_ACTION_REMOVE_EMAIL_DOMAIN:
        return RemoveEmailDomain()
    elif petal_action == shared.PETAL_ACTION_ADD_EMAIL_ALIAS:
        return AddEmailAlias()
    elif petal_action == shared.PETAL_ACTION_REMOVE_EMAIL_ALIAS:
        return RemoveEmailAlias()
    elif petal_action == shared.PETAL_ACTION_CREATE_WEBSTORE_SITE:
        return CreateWebstoreSite()
    elif petal_action == shared.PETAL_ACTION_DISABLE_WEBSTORE_SITE:
        return DisableWebstoreSite()
    elif petal_action == shared.PETAL_ACTION_DELETE_WEBSTORE_SITE:
        return DeleteWebstoreSite()

    log_error("Resource factory has no class for petal_action {0}".format(petal_action))

    return None


class ResourcerCommon():
    data = {}
    nonce = None
    resource_class = None
    cmd_path = BASE_DIR + "/flowerpetal/bin/"

    # The tmp directory is hard-coded here and also hard-coded in the bin
    # scripts for security reasons. It ensures that the bin scripts (which are
    # typically run using sudo) will only act on files in this one directory
    # which should be owned by the user running the flower petal.
    tmp = "/var/lib/flower/tmp/"

    def initialize(self, resource_class, nonce, data):
        self.data = json.loads(data)
        self.nonce = nonce
        self.resource_class = resource_class
        thread = threading.Thread(target=self.run)
        thread.start()

    def run(self):
        if self.verify_nonce():
            log_debug("Successfully verified nonce")
            try:
               self.process() 
            except Exception as e:
                self.submit_error(repr(e))

        else:
            # If we can't verify the nonce, we can't (and don't want to) submit
            # an error. So, this just has to live in the logs and if there is a
            # valid resource, it will remain in In Progress status until it is
            # re-submitted.
            log_error("Failed to verify nonce ({0})".format(self.nonce))

    def process(self):
        raise RuntimeError("Please implement process() in your petal action class.")

    def verify_nonce(self):
        # Check with the hub to make sure the nonce is valid.
        data = {
            'resource_class': self.resource_class,
            'nonce': self.nonce 
        }
        # Kill me please. I think we have a race condition when we are
        # using sqlite - the nonce is saved in the database, but may not
        # be flushed to disk. I sometimes get a failure here, so sleeping
        # for a few seconds.
        if FLOWER_MODE == FLOWER_MODE_DEV or FLOWER_MODE == FLOWER_MODE_TEST:
            time.sleep(5)
        log_debug("Trying to verify nonce {0} and resource class {1}.".format(self.nonce, self.resource_class))
        return self.submit(data, 'verify-nonce')


    def submit_error(self, msg):
        log_error("Submitting error: {0}".format(msg))
        data = {
            'resource_class': self.resource_class,
            'nonce': self.nonce,
            'error': msg
        }
        self.submit(data, 'update-resource-status')


    def submit_success(self):
        data = {
            'resource_class': self.resource_class,
            'nonce': self.nonce,
        }
        log_debug("Submitting success, nice work.")
        self.submit(data, 'update-resource-status')

    # Run the specified command locally, return the exit code.
    def execute(self, cmd, sudo = True):
        if FLOWER_MODE == FLOWER_MODE_DEV:
            cmd.insert(0, 'echo')
        elif sudo:
            cmd.insert(0, 'sudo')

        log_debug("About to execute commnd...")
        log_debug(cmd)
        result = subprocess.run(cmd)
        log_debug("Returning returncode {0} for command {1}".format(result.returncode, cmd))
        return result.returncode

    # Takes string content, writes to random file in pre-selected directory
    # and returns the file name.
    def write_to_temp_file(self, content):
        temp=tempfile.NamedTemporaryFile(mode="w+", delete=False, dir=self.tmp)
        temp.write(content)
        temp.close()
        return os.path.basename(temp.name)

    # Given a template file and a dictionary context, render the
    # file and drop it in our temp directory, returning the name
    # of the tmp file created.
    def render_template(self, template_file, context):
        try:
            t = get_template(template_file)
            out = t.render(context) 
            return self.write_to_temp_file(out)
        except Exception as e:
            raise RuntimeError("Error rendering template: {0}".format(e))

    def cleanup(self, filename):
        file_path= self.tmp + filename
        file_obj = Path(file_path)
        if file_obj.is_file():
            os.remove(file_path)

    """
    Submit a request to the hub. Return True if we get a 200 response.

    """
    def submit(self, data, endpoint):
        if endpoint == 'update-resource-status':
            url = FLOWER_BASE_URL + "api/update-resource-status"
        elif endpoint == 'verify-nonce':
            url = FLOWER_BASE_URL + "api/verify-nonce"
        else:
            log_error("Unknown endpoint {0}".format(endpoint))
            return False

        log_debug("Posting to {0}".format(url))
        r = requests.post(url, data=data, verify="/etc/ssl/certs/ca-certificates.crt")
        if r.status_code != 200:
            log_error("Got bad status code via url '{0}'. Code: {1}.".format(url, r.status_code))
            return False

        return True  

class NoOp(ResourcerCommon):
    def process(self):
        self.submit_success()

class LdapCommon:
    tld = None

    def derive_tld(self):
        if FLOWER_MODE == FLOWER_MODE_DEV:
            self.tld = 'dev'
        elif FLOWER_MODE == FLOWER_MODE_TEST:
            self.tld = 'test'
        else:
            # Derive the tld from the FLOWER_BASE_URL
            m = re.search('([a-z]+)/?$', FLOWER_BASE_URL)
            self.tld = m.group(1)

    """
    We have several ldap commands that check if a record exists in a particular
    way. They all return 0 if the answer is yes, 1 if the answer is no, and
    greater then 1 if there is an error. This functon provdes the basic logic
    for all of them.
    """
    def exists(self, cmd):
        ret = self.execute(cmd)

        if ret > 1:
            raise RuntimeError('The ldapsearch command had an error.')

        if ret == 0:
            return True
        return False

    def email_exists(self, username, email):
        cmd = [ self.cmd_path + "/ldap-email-exists", username, email ]
        return self.exists(cmd)

    def email_domain_exists(self, domain_name):
        cmd = [ self.cmd_path + "/ldap-email-domain-exists", domain_name ]
        return self.exists(cmd)

    def email_alias_exists(self, email):
        cmd = [ self.cmd_path + "/ldap-email-alias-exists", email ]
        return self.exists(cmd)

    def class_exists(self, username, class_name):
        cmd = [ self.cmd_path + "/ldap-class-exists", username, class_name ]
        return self.exists(cmd)

    def user_exists(self, username):
        cmd = [ self.cmd_path + "/ldap-user-exists", username ]
        return self.exists(cmd)

    # Used by delete() and disable()
    def delete_uid(self, uid):
        log_debug("Deleting ldap uid {0}.".format(uid))
        cmd = [ self.cmd_path + "ldap-delete-user", uid ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to delete ldap user.')

    # Remove an email address from the usr OR remove the last email address (in 
    # which case, remove the postfixAccount object class as well)
    def remove_email_address(self, last=True):
        log_debug("Removing email address {0} for {1}.".format(self.data['email'], self.data["username"]))

        if not self.user_exists(self.data['username']):
            log_debug("LDAP user {0} already gone.".format(self.data["username"]))
            return

        if not self.email_exists(self.data['username'], self.data['email']):
            log_debug("Email address {0} for {1} already gone.".format(self.data['email'], self.data["username"]))
            return
                
        context = self.data
        context['tld'] = self.tld
        if last:
            filename = self.render_template("ldap-remove-last-email-address.ldif", context)
        else:
            filename = self.render_template("ldap-remove-email-address.ldif", context)

        # Build command.
        cmd = [ self.cmd_path + "ldap-modify", filename ]
        
        ret = self.execute(cmd)
        if not ret == 0:
            self.cleanup(filename)
            raise RuntimeError('Failed to remove email address from ldap user.')
        log_debug("Success, removed email {0} from user {1}".format(self.data['email'], self.data["username"]))
        self.cleanup(filename)

class CreateLdapLogin(ResourcerCommon, LdapCommon):

    def process(self):
        log_debug("Creating Login LDAP record for {0}.".format(self.data["username"]))

        self.derive_tld()

        # Build command to create or modify the user.
        cmd = []
        # User exists, modify
        if self.user_exists(self.data['username']):
            log_debug("User {0} exists, modifying.".format(self.data["username"]))
            cmd.append(self.cmd_path + "ldap-modify")
            template_file = "ldap-modify-user.ldif"
        else:
            log_debug("User {0} does not exist, adding.".format(self.data["username"]))
            # New user, add
            cmd.append(self.cmd_path + "ldap-add")
            template_file = "ldap-add-user.ldif"
        
        context = self.data
        context['tld'] = self.tld

        # Last name is optional in the control panel (as it should be) but
        # required by LDAP inetOrgPerson, so set it to the first name so we
        # have a value.
        if not context["last_name"]:
            context["last_name"] = context["first_name"]

        filename = self.render_template(template_file, context)
        cmd.append(filename)
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to add/modify ldap user.')
        log_debug("Success fully created {0}".format(self.data["username"]))

        self.submit_success()
        self.cleanup(filename)

class DisableLdapLogin(ResourcerCommon, LdapCommon):
    def process(self):
        log_debug("Disabling Login LDAP record for {0}.".format(self.data["username"]))
        self.delete_uid(self.data["username"]) 
        self.submit_success()

class DeleteLdapLogin(ResourcerCommon, LdapCommon):
    def process(self):
        log_debug("Deleting Login LDAP record for {0}.".format(self.data["username"]))
        self.delete_uid(self.data["username"]) 
        self.submit_success()


class CreateDomainZone(ResourcerCommon):
    def process(self):
        log_debug("Creating DomainZone record for {0}.".format(self.data["zone"]))
        
        # In short, we ensure a <zone>.conf file is in place in /etc/knot, we ensure
        # a <zone>.zone file is in place in /var/lib/knot, we check syntax and we reload.

        # Handle the zone file.
        zone_file = self.render_template('knot.zone', self.data)

        # Check syntax
        cmd = [ "/usr/bin/kzonecheck", "-o", self.data['zone'], self.tmp + zone_file ]
        log_debug("Checking with kzonecheck")
        ret = self.execute(cmd, sudo = False)
        if not int(ret) == 0:
            # Let's leave behind the temp file so we can run kzonecheck and find out why it failed.
            #self.cleanup(zone_file)
            raise RuntimeError('Zone file failed kzonecheck for zone {0}'.format(self.data["zone"]))

        # Move zone file into place.
        cmd = [ self.cmd_path + "knot-zone", zone_file, self.data['zone'] ]
        ret = self.execute(cmd)
        if not ret == 0:
            self.cleanup(zone_file)
            raise RuntimeError('Failed to move knot zone file into place for zone {0}'.format(self.data["zone"]))
        log_debug("Successfully moved knot zone file into place for zone {0}".format(self.data["zone"]))
        self.cleanup(zone_file)

        # Handle the conf file.
        context = { 'zone': self.data['zone'] }
        conf_file = self.render_template('knot.conf', context)
        cmd = [ self.cmd_path + "knot-conf", conf_file, self.data['zone'] ]
        ret = self.execute(cmd)
        if not ret == 0:
            self.cleanup(conf_file)
            raise RuntimeError('Failed to move knot conf file into place for zone {0}'.format(self.data["zone"]))
        log_debug("Successfully moved knot conf file into place for zone {0}".format(self.data["zone"]))
        self.cleanup(conf_file)

        # Reload knot
        cmd = [ self.cmd_path + "knot-reload" ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to reload knot for zone {0}'.format(self.data["zone"]))
        log_debug("Successfully reloaded knot for zone {0}".format(self.data["zone"]))

        # Done.
        self.submit_success()

class DeleteDomainZone(ResourcerCommon):
    def process(self):
        cmd = [ self.cmd_path + "knot-delete", self.data['zone'] ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError("Failed to delete zone file.")

        # Reload knot
        cmd = [ self.cmd_path + "knot-reload" ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to reload knot for zone {0}'.format(self.data["zone"]))

        log_debug("Successfully reloaded knot for zone {0}".format(self.data["zone"]))

        self.submit_success()

class AddEmailAddress(ResourcerCommon, LdapCommon):
    def process(self):
        self.derive_tld()

        log_debug("Adding email address for {0}.".format(self.data["username"]))

        if self.email_exists(self.data['username'], self.data['email']):
            log_debug("Email address {0} for {1} already exists.".format(self.data['email'], self.data["username"]))
            self.submit_success()
            return

        # Email does not exist, add it.
        context = self.data
        context['tld'] = self.tld
        if self.class_exists(self.data['username'], 'postfixAccount'):
            # We are already a PostfixAccount, just add the email.
            log_debug("User {0} already has PostfixAccount .".format(self.data["username"]))
            filename = self.render_template("ldap-add-email-address.ldif", context)
        else:
            # Add the email + PostfixAccount class + mailBox.
            log_debug("User {0} does not have PostfixAccount .".format(self.data["username"]))
            filename = self.render_template("ldap-add-first-email-address.ldif", context)

        # Build command.
        cmd = [ self.cmd_path + "ldap-modify", filename ]
        
        ret = self.execute(cmd)
        if not ret == 0:
            self.cleanup(filename)
            raise RuntimeError('Failed to add email address to ldap user.')
        log_debug("Success, added email {0} to user {1}".format(self.data['email'], self.data["username"]))

        self.submit_success()
        self.cleanup(filename)

class RemoveLastEmailAddress(ResourcerCommon, LdapCommon):
    def process(self):
        self.derive_tld()
        self.remove_email_address(last=True) 
        self.submit_success()

class RemoveEmailAddress(ResourcerCommon, LdapCommon):
    def process(self):
        self.derive_tld()
        self.remove_email_address(last=False) 
        submit_success()
     
class CreateMailDir(ResourcerCommon):
    def process(self):
        cmd = [ self.cmd_path + "maildir-create", self.data["username"], self.data["partition"] ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to create maildir for username {0}, partition {1}'.format(self.data["username"], self.data['partition']))

        log_debug("Successfully created maildir for {0}.".format(self.data["username"]))
        self.submit_success()
        
class DeleteMailDir(ResourcerCommon):
    def process(self):
        cmd = [ self.cmd_path + "maildir-delete", self.data["username"], self.data["partition"] ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to delete maildir for username {0}, partition {l}'.format(self.data["username"], self.data['partition']))

        log_debug("Successfully deleted maildir for {0}.".format(self.data["username"]))
        self.submit_success()

class AddEmailDomain(ResourcerCommon, LdapCommon):
    def process(self):
        log_debug("Creating LDAP email domain {0}.".format(self.data["domain_name"]))

        self.derive_tld()

        # Check if email domain exists. 
        if self.email_domain_exists(self.data['domain_name']):
            log_debug("Email Domain {0} exists, we are done".format(self.data["domain_name"]))
            self.submit_success()
            return

        context = self.data
        context['tld'] = self.tld
        filename = self.render_template("ldap-add-email-domain.ldif", context)
        cmd = [ self.cmd_path + "ldap-add",  filename]
        ret = self.execute(cmd)
        if not ret == 0:
            #self.cleanup(filename)
            raise RuntimeError('Failed to add email domain.')
        log_debug("Success fully created email domain {0}".format(self.data["domain_name"]))
        self.submit_success()
        self.cleanup(filename)

class RemoveEmailDomain(ResourcerCommon, LdapCommon):
    def process(self):
        log_debug("Deleting LDAP email domain {0}.".format(self.data["domain_name"]))

        cmd = [ self.cmd_path + "ldap-delete-email-domain", self.data['domain_name']]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to delete email domain.')
        log_debug("Success fully deleted email domain {0}".format(self.data["domain_name"]))
        self.submit_success()

class AddEmailAlias(ResourcerCommon, LdapCommon):
    def process(self):
        log_debug("Adding LDAP email alias {0}.".format(self.data["email"]))
        self.derive_tld()

        # Check if email alias exists. 
        if self.email_alias_exists(self.data['email']):
            log_debug("Email {0} exists, we are done".format(self.data["email"]))
            self.submit_success()
            return

        context = self.data
        context['tld'] = self.tld
        filename = self.render_template("ldap-add-email-alias.ldif", context)
        cmd = [ self.cmd_path + "ldap-add",  filename]
        ret = self.execute(cmd)
        if not ret == 0:
            #self.cleanup(filename)
            raise RuntimeError('Failed to add email alias.')
        log_debug("Successfully created email alias {0}".format(self.data["email"]))
        self.submit_success()

class RemoveEmailAlias(ResourcerCommon, LdapCommon):
    def process(self):
        log_debug("Deleting LDAP email alias {0}.".format(self.data["email"]))
        cmd = [ self.cmd_path + "ldap-delete-email-alias", self.data['email']]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to delete email alias.')
        log_debug("Successfully deleted email alias {0}".format(self.data["email"]))
        self.submit_success()

class WebstoreSiteCommon():
    def delete_apache2_conf(self):
        log_debug("Deleting apache2 web config for site {0}.".format(self.data["id"]))
        cmd = [ self.cmd_path + "apache-conf-remove", str(self.data["id"])]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError("Failed to remove apache2 website configuration for site id {0}".format(self.data["id"]))
        log_debug("Successfully removed website configuration for site id {0}".format(self.data["id"]))

    def delete_phpfpm_conf(self, version=None):
        deletionlist=[]
        if version:
            deletionlist=[version]
        else:
            deletionlist=self.data["php_choices"] 
        for number in deletionlist :
            cmd = [ self.cmd_path + "phpfpm-conf-remove", str(self.data["id"]), str(number) ]
            ret = self.execute(cmd)
            if not ret == 0:
                raise RuntimeError("Failed to remove php-fpm configuration version ${1} for site id {0}".format(self.data["id"]), str(number) )    
        log_debug("Successfully removed all php-fpm configurations for site id {0}".format(self.data["id"]))


class CreateWebstoreSite(ResourcerCommon,WebstoreSiteCommon):
    def process(self):
        # We create a new directory for the website and
        # create an apache config file and copy that into place

        log_debug("Printing contents of data for debug purposes{0}".format(self.data))

        #Create the webstore directory
        cmd = [ self.cmd_path + "webstore-site-create", str(self.data["id"]), self.data["partition"] ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to create webstore directory on partition {1}'.format(self.data["id"], self.data['partition']))
        log_debug("Successfully created webstore directory for site id {0}.".format(self.data["id"]))

        # Pre-test apache config tree
        cmd = [ self.cmd_path + "apache-conf-test"]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Existing pache configuration tree test failed, no changes made')
        log_debug("Preliminary apache configuration tree test passed.")


        # Create the apache config file
        apache_config_file = self.render_template('apache.conf', self.data)
        cmd = [ self.cmd_path + "apache-conf-create", str(self.data["id"]) , apache_config_file ]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to create apache website configuration for site id {0}'.format(self.data["id"]))
        log_debug("Successfully created apache website configuration for site id {0}.".format(self.data["id"]))


        # Test apache config tree
        cmd = [ self.cmd_path + "apache-conf-test"]
        ret = self.execute(cmd)
        if not ret == 0:
            log_debug('Apache configuration tree test failed')
            self.delete_apache2_conf()
            raise RuntimeError('Apache configuration tree test failed')
        log_debug("Apache configuration tree test passed.")

        # Reload apache
        cmd = [ self.cmd_path + "apache-reload"]
        ret = self.execute(cmd)
        if not ret == 0:
            log_debug('Reloading apache2 web server failed')
            self.delete_apache2_conf()
            raise RuntimeError('Reloading apache2 web server failed')
        log_debug("Reloading apache2 web server was successful.")

        #we should always delete any lingering php-fpm config files. User might have switched php version or disbaled php processes
        self.delete_phpfpm_conf()

        # Create the php-fpm config file if php_processes are greater than 0
        if self.data['php_processes'] :
            php_fpm_pool_file = self.render_template('php-fpm.conf',self.data)
            #log_debug("Printing php-fpm config file {0}".format(self.tmp+php_fpm_pool_file))

             # Pre-test php-fpm config tree
            cmd = [ self.cmd_path + "phpfpm-conf-test", str(self.data["php_version"])]
            ret = self.execute(cmd)
            if not ret == 0:
                raise RuntimeError('Preliminary php-fpm{0} configuration tree test failed'.format(self.data["php_version"]))
            log_debug("Preliminary php-fpm{0} configuration tree test passed.".format(self.data["php_version"]))           

            # Create php-fpm pool config
            cmd = [ self.cmd_path + "phpfpm-conf-create", str(self.data["id"]) , str(self.data["php_version"]), php_fpm_pool_file ]
            ret = self.execute(cmd)
            if not ret == 0:
                raise RuntimeError('Failed to create php-fpm configuration for site id {0}'.format(self.data["id"]))
            log_debug("Successfully created php-fpm configuration for site id {0}.".format(self.data["id"]))

            # Test php-fpm config tree
            cmd = [ self.cmd_path + "phpfpm-conf-test", str(self.data["php_version"])]
            ret = self.execute(cmd)
            if not ret == 0:
                log_debug('php-fpm{0} configuration tree test failed'.format(self.data["php_version"]))
                self.delete_phpfpm_conf(self.data["php_version"])
                raise RuntimeError('php-fpm{0} configuration tree test failed'.format(self.data["php_version"]))
            log_debug("php-fpm{0} configuration tree test passed.".format(self.data["php_version"]))

            # Reload php-fpm
            cmd = [ self.cmd_path + "phpfpm-reload", str(self.data["php_version"])]
            ret = self.execute(cmd)
            if not ret == 0:
                log_debug('Reloading of php-fpm{0} failed'.format(self.data["php_version"]))
                self.delete_phpfpm_conf(self.data["php_version"])
                raise RuntimeError('Reloading of php-fpm{0} failed'.format(self.data["php_version"]))
            log_debug("Reloading of php-fpm{0} was successful.".format(self.data["php_version"]))
           
        self.submit_success()


class DisableWebstoreSite(ResourcerCommon):
    def process(self):
        self.delete_apache2_conf()
        self.delete_phpfpm_conf()
        self.submit_success()

class DeleteWebstoreSite(ResourcerCommon):
    def process(self):
        self.delete_apache2_conf()
        self.delete_phpfpm_conf()
        cmd = [ self.cmd_path + "webstore-site-delete", str(self.data["id"]), self.data['partition']]
        ret = self.execute(cmd)
        if not ret == 0:
            raise RuntimeError('Failed to delete webstore directory and files for site id {0} on partition {1}'.format(self.data["id"], self.data['partition']))
        log_debug("Successfully deleted webstore directory and files for site id {0} on partition {1}.".format(self.data["id"], self.data['partition']))
        self.submit_success()
